<?php
 
include("includes/smarty.inc.php"); 
include("includes/config.inc.php"); 
include("includes/fonctions.inc.php"); 

$valeurs = array();

$db = new mysqli($db_host, $db_user, $db_pwd, $db_name);

	$requete = "SELECT timestamp, t1, t2, t3, t4, t7, t8, t9, t11, v3v, c1, c4, c5, chaudiere FROM solisgraph
					WHERE timestamp = (SELECT MAX(timestamp) FROM solisgraph)";
	$results = $db->query($requete);

	while($row = $results->fetch_assoc())
	{
	    $timestamp = $row['timestamp'];
	    $valeurs['t1'] = str_replace('.',',',$row['t1']).'°C';
	    $valeurs['t2'] = str_replace('.',',',$row['t2']).'°C';
	    $valeurs['t3'] = str_replace('.',',',$row['t3']).'°C';
	    $valeurs['t4'] = str_replace('.',',',$row['t4']).'°C';
	    $valeurs['t7'] = str_replace('.',',',$row['t7']).'°C';
	    $valeurs['t8'] = str_replace('.',',',$row['t8']).'°C';
	    $valeurs['t9'] = str_replace('.',',',$row['t9']).'°C';
	    $valeurs['t11'] = str_replace('.',',',$row['t11']).'°C';
	    $valeurs['v3v'] = $row['v3v'];
	    $valeurs['c1'] = $row['c1']."%";
	    $valeurs['c4'] = $row['c4']."%";
	    $valeurs['c5'] = $row['c5']."%";
	    $valeurs['chaudiere'] = $row['chaudiere'];
	}

	$valeurs['timestamp'] = date("d/m/Y H:i:s", $timestamp);
	$valeurs['maintenant'] = date("d/m/Y H:i:s");

// Depuis quand la chaudière est-elle éteinte ?

$requete = "SELECT timestamp FROM solisgraph
					WHERE timestamp = (SELECT MAX(timestamp) FROM solisgraph WHERE chaudiere = 50)";
	$results = $db->query($requete);

	while($row = $results->fetch_assoc())
	    $derniere_chauffe = $row['timestamp'];
	
	// Si la chaudière était en route il y a moins de 20mn
	$difference_chaudiere = time()-$derniere_chauffe;
	if( $difference_chaudiere < 1200 )
		$chaudiere_chaude = true;
	else
		$chaudiere_chaude = false;

// Extract Weathermap data 

    $requete = "SELECT * FROM weathermap
					WHERE timestamp = (SELECT MAX(timestamp) FROM weathermap)";

	$results = $db->query($requete);

$res = mysqli_fetch_assoc($results);

    $valeurs['temp_cur']=str_replace('.',',',$res['temp_cur']);
    $valeurs['temp_feels_like_cur']=str_replace('.',',',$res['temp_feels_like_cur']);
    $valeurs['humidity_cur']=$res['humidity_cur'];
    $valeurs['dew_point_cur']=str_replace('.',',',$res['dew_point_cur']);
    $valeurs['wind_deg_cur']=direction_vent_precis($res['wind_deg_cur']);
    $valeurs['wind_speed_cur']=$res['wind_speed_cur'];
    $valeurs['wind_gust_cur']=$res['wind_gust_cur'];

    $valeurs['temp_min_0']=$res['min_0'];
    $valeurs['temp_max_0']=$res['max_0'];
    $valeurs['humidity_0']=$res['humidity_0'];
    $valeurs['wind_speed_0']=$res['wind_0'];
    $valeurs['wind_deg_0']=direction_vent_precis($res['wind_deg_0']);
    $valeurs['w_id_00']=$res['id_00'];
    $valeurs['w_description_00']=$res['description_00'];
    $valeurs['current_time_6']=$res['dt_01'];
    $valeurs['w_id_01']=$res['id_01'];
    $valeurs['w_description_01']=$res['description_01'];
    $valeurs['current_time_12']=$res['dt_02'];
    $valeurs['w_id_02']=$res['id_02'];
    $valeurs['w_description_02']=$res['description_02'];
    $valeurs['rain_0']=$res['rain_0'];
    $valeurs['rain_pop_0']=$res['rain_pop_0'];
    $valeurs['uv_0']=$res['uv_0'];
    $valeurs['dt_0']=$res['dt_0'];  
    $valeurs['sunrise_0']=$res['sunrise_0'];  
    $valeurs['sunset_0']=$res['sunset_0'];  

    $valeurs['temp_min_1']=$res['min_1'];
    $valeurs['temp_max_1']=$res['max_1'];
    $valeurs['humidity_1']=$res['humidity_1'];
    $valeurs['wind_speed_1']=$res['wind_1'];
    $valeurs['wind_deg_1']=direction_vent_precis($res['wind_deg_1']);
    $valeurs['w_id_1']=$res['id_1'];
    $valeurs['w_description_1']=$res['description_1'];
    $valeurs['rain_1']=$res['rain_1'];
    $valeurs['rain_pop_1']=$res['rain_pop_1'];
    $valeurs['dt_1']=$res['dt_1']; 

    $valeurs['temp_min_2']=$res['min_2'];
    $valeurs['temp_max_2']=$res['max_2'];
    $valeurs['humidity_2']=$res['humidity_2'];
    $valeurs['wind_speed_2']=$res['wind_2'];
    $valeurs['wind_deg_2']=direction_vent_precis($res['wind_deg_2']);
    $valeurs['w_id_2']=$res['id_2'];
    $valeurs['w_description_2']=$res['description_2'];
    $valeurs['rain_2']=$res['rain_2'];
    $valeurs['rain_pop_2']=$res['rain_pop_2'];
    $valeurs['dt_2']=$res['dt_2']; 

    $valeurs['temp_min_3']=$res['min_3'];
    $valeurs['temp_max_3']=$res['max_3'];
    $valeurs['humidity_3']=$res['humidity_3'];
    $valeurs['wind_speed_3']=$res['wind_3'];
    $valeurs['wind_deg_3']=direction_vent_precis($res['wind_deg_3']);
    $valeurs['w_id_3']=$res['id_3'];
    $valeurs['w_description_3']=$res['description_3'];
    $valeurs['rain_3']=$res['rain_3'];
    $valeurs['rain_pop_3']=$res['rain_pop_3'];
    $valeurs['dt_3']=$res['dt_3']; 

    $valeurs['feels_like_0']=$res['feels_like_0'];
    $valeurs['timestamp_wm']=$res['timestamp'];

    $valeurs['loc_name'] = $loc_name;

// Prochain lever de soleil

$valeurs['next_sunrise']=$valeurs['sunrise_0']+86400;

// Création des icones meteo tenant compte de la nuit ou du jour 

$night=false;
if ($valeurs['timestamp_wm']<$valeurs['sunrise_0'] OR ($valeurs['timestamp_wm']>$valeurs['sunset_0'] && $valeurs['timestamp_wm']<$valeurs['next_sunrise']))
        $night=true;
$valeurs['icon_00']=define_weather_icon($valeurs['w_id_00'],$night,$db);

$night=false;
if ($valeurs['current_time_6']<$valeurs['sunrise_0'] OR ($valeurs['current_time_6']>$valeurs['sunset_0']&& $valeurs['current_time_6']<$valeurs['next_sunrise']))
        $night=true;
$valeurs['icon_01']=define_weather_icon($valeurs['w_id_01'],$night,$db);

$night=false;
if ($valeurs['current_time_12']<$valeurs['sunrise_0'] OR ($valeurs['current_time_12']>$valeurs['sunset_0']&& $valeurs['current_time_12']<$valeurs['next_sunrise']))
        $night=true;
$valeurs['icon_02']=define_weather_icon($valeurs['w_id_02'],$night,$db);

$night=false;
$valeurs['icon_1']=define_weather_icon($valeurs['w_id_1'],$night,$db);
$valeurs['icon_2']=define_weather_icon($valeurs['w_id_2'],$night,$db);
$valeurs['icon_3']=define_weather_icon($valeurs['w_id_3'],$night,$db);

// Est-ce que les demi-journées sont pour aujourd'hui ou demain ?

$minuit = strtotime("today 23:59");
if ($valeurs['current_time_6'] > $minuit)
	$jour_6 = "Demain";
if ($valeurs['current_time_12'] > $minuit)
	$jour_12 = "Demain";

// Formatage des heures au format hh:mm:ss

$valeurs['sunrise_hm']= date("H:i", $valeurs['sunrise_0']);
$valeurs['sunset_hm']= date("H:i", $valeurs['sunset_0']);
$valeurs['sunrise_0']= date("H:i:s", $valeurs['sunrise_0']);
$valeurs['sunset_0']= date("H:i:s", $valeurs['sunset_0']);
$valeurs['current_time_6']= $jour_6." ".date("H", $valeurs['current_time_6']);
$valeurs['current_time_12']= $jour_12." ".date("H", $valeurs['current_time_12']);
$valeurs['current_time']=date("H:i:s", $valeurs['timestamp_wm']);

// Encodage du nom des jours suivants 

$valeurs['timestamp'] = date("d/m/Y H:i:s", $timestamp);
$valeurs['heure_wm'] = date("H:i", $valeurs['timestamp_wm']);
$valeurs['timestamp_wm'] = date("d/m/Y H:i:s", $valeurs['timestamp_wm']);

$valeurs['maintenant']=ucwords(strftime('%A %e %B %Y'));

$valeurs['day_0']=ucfirst(strftime("%A",$valeurs['dt_0']));
$valeurs['day_1']=ucfirst(strftime("%A",$valeurs['dt_1']));
$valeurs['day_2']=ucfirst(strftime("%A",$valeurs['dt_2']));
$valeurs['day_3']=ucfirst(strftime("%A",$valeurs['dt_3']));

// Test températures glaciales
/*
if (test_temperature($valeurs['feels_like_0'],$valeurs['wind_speed_0'],$valeurs['wind_deg_0'],$valeurs['w_description_0']))
{
    // $valeurs['alert']="&#x2744;";
    $valeurs['alert']="</br> ".ucwords("Journée froide");
}
*/
// Affichage d'une alerte si la différence est importante

function rawsrc($argu)
{
	$base = function($ba,$se) { return ($ba >= $se) ? array($ba % $se, floor($ba / $se)) : array($ba, 0); };
	$seco = $base($argu,60);
	$minu = $base($seco[1],60);
	$heur = $base($minu[1],24);
	return sprintf("%01dj %02d:%02d:%02d", $heur[1], $heur[0], $minu[0], $seco[0]);
}

	$difference = time() - $timestamp;
	$valeurs['difference'] = rawsrc($difference);

	if ($difference > 300) // 5 minutes
		$valeurs['alerte'] = 'Attention, aucune donnée depuis '.$valeurs['difference']."<p></p>";

// Affichage des puces

    if ($valeurs['c1']=="0%")    
        $valeurs['c1']="<img src=image/circle-red.png>";
    else
        $valeurs['c1']="<img src=image/circle-green.png>&nbsp;".$valeurs['c1']."<br/>".$valeurs['t8']."&nbsp;&nbsp;".$valeurs['t7'];

    if ($valeurs['c5']=="0%")
        $valeurs['c5']="<img src=image/circle-red.png>";    
    else
        $valeurs['c5']="<img src=image/circle-green.png>&nbsp;".$valeurs['c5'];

    if ($valeurs['c4']=="0%")
        $valeurs['c4']="<img src=image/circle-red.png>";    
    else
        $valeurs['c4']="<img src=image/circle-green.png>&nbsp;".$valeurs['c4'];

    if ($valeurs['chaudiere'] == 50)
    	$valeurs['chaudiere'] = '<img src=image/circle-green.png>';
    else
    	if ($chaudiere_chaude)
    		$valeurs['chaudiere'] = '<img src=image/circle-orange.png>';
    	else
	    	$valeurs['chaudiere'] = '<img src=image/circle-red.png>';


// Formatage description temps

$valeurs['w_description_00']=format_weather_type($valeurs['w_description_00']);
$valeurs['w_description_01']=format_weather_type($valeurs['w_description_01']);
$valeurs['w_description_02']=format_weather_type($valeurs['w_description_02']);
$valeurs['w_description_1']=format_weather_type($valeurs['w_description_1']);
$valeurs['w_description_2']=format_weather_type($valeurs['w_description_2']);
$valeurs['w_description_3']=format_weather_type($valeurs['w_description_3']);

// Envoi du template

$tpl->assign("valeurs",$valeurs);
$tpl->assign("numero_installation",$numero_installation);
//$tpl->display("visualisation.tpl");
$tpl->display("visualisation-min.tpl");

?>