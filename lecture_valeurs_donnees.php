<?php

include("includes/config.inc.php"); 


$cookie = "solisart.cookie";

// --------------------------------
// FONCTION D'AUTHENTIFICATION
// --------------------------------

function authentifier($cookie,$serveur_solisart,$login,$passwd)
{
	// on "force" en POST les variables d'identification et on stocke le cookie obtenu.
	$ch = curl_init();
	
	curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
	curl_setopt($ch, CURLOPT_URL, $serveur_solisart."/admin/index.php");
	curl_setopt($ch, CURLOPT_POST, 1);
	$post_fields = "connexion=Connexion&id=".$login."&pass=".$passwd;
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);

	curl_exec ($ch);
	curl_close ($ch);
}

// --------------------------------
// FONCTION DE LECTURE DES DONNÉES
// --------------------------------

function lire_donnees($cookie,$serveur_solisart,$heure,$numero_installation)
{
	
	$ch = curl_init();
	
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
	curl_setopt($ch, CURLOPT_URL, $serveur_solisart."/admin/divers/ajax/lecture_valeurs_donnees.php");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "id=".base64_encode($numero_installation)."&heure=".$heure."&periode=1");
	
	$result = curl_exec ($ch);
	curl_close ($ch);
	
	// Récupération des données XML
	$donnees = simplexml_load_string($result);

	$retour[0] = $result;
	$retour[1] = $donnees;

	return $retour;

}

// --------------------------------
// DÉBUT
// --------------------------------

	if (isset($_GET['heure']) and is_numeric($_GET['heure']))
	{
		$heure = $_GET['heure'];

		$retour = lire_donnees($cookie,$serveur_solisart,$heure,$numero_installation);
		
		if (!($retour[1] === false))	
		{
			// Récupération du statut
			$statut = $retour[1]->attributes()->statut;
		
			// Si échec on fait l'authentification et on relit les données
			if ($statut == 'echec')
			{
				authentifier($cookie,$serveur_solisart,$login,$passwd);
				$retour = lire_donnees($cookie,$serveur_solisart,$heure,$numero_installation);
			}
		
			$timestamp = intval($retour[1]->attributes()->heure_contact);
			echo $retour[0];
		}
	}
	else
		die("Erreur - Parametre abent ou erronne"); 

?>