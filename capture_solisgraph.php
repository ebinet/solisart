<?php

include("includes/config.inc.php"); 

$db = new mysqli($db_host, $db_user, $db_pwd, $db_name);

$cookie = "solisart.cookie";

$variables = array (
	584 => 't1',
	585 => 't2',
	586 => 't3',
	587 => 't4',
	590 => 't7',
	591 => 't8',
	592 => 't9',
	594 => 't11',
	606 => 'chaudiere',
	614 => 'c4',
	615 => 'c5',
	618 => 'c1',
	628 => 'v3v',
	631 => 'tcons1',
	985 => 'timestamp'
);

// --------------------------------
// FONCTION D'AUTHENTIFICATION
// --------------------------------

function authentifier($cookie,$serveur_solisart,$login,$passwd)
{
	// on "force" en POST les variables d'identification et on stocke le cookie obtenu.
	$ch = curl_init();
	
	curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
	curl_setopt($ch, CURLOPT_URL, $serveur_solisart."/admin/index.php");
	curl_setopt($ch, CURLOPT_POST, 1);
	$post_fields = "connexion=Connexion&id=".$login."&pass=".$passwd;
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);

	curl_exec ($ch);
	curl_close ($ch);
}

// --------------------------------
// FONCTION DE LECTURE DES DONNÉES
// --------------------------------

function lire_donnees($cookie,$serveur_solisart,$numero_installation)
{
	
	$ch = curl_init();
	
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
	curl_setopt($ch, CURLOPT_URL, $serveur_solisart."/admin/divers/ajax/lecture_valeurs_donnees.php");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "id=".base64_encode($numero_installation)."&heure=".strtotime("-1 day")."&periode=1");
	
	$result = curl_exec ($ch);
	curl_close ($ch);
	
	// Récupération des données XML
	$donnees = simplexml_load_string($result);

	return $donnees;

}

// --------------------------------
// DÉBUT
// --------------------------------

// Par défaut on appelle la lecture des données sans authentification
// car on présume que c'est déjà fait

$insert_string_fields = '';
$insert_string_values = '';

$donnees = lire_donnees($cookie,$serveur_solisart,$numero_installation);
if (!($donnees === false))
	$statut = $donnees->attributes()->statut;
else
	$statut = 'echec';

// Si échec on fait l'authentification et on relit les données
if ($statut == 'echec')
{
	authentifier($cookie,$serveur_solisart,$login,$passwd);
	
	$donnees = lire_donnees($cookie,$serveur_solisart,$numero_installation);
	if (!($donnees === false))
		$statut = $donnees->attributes()->statut;
	else
		$statut = 'echec';
}

//	var_dump($donnees); echo "<br />";

if ($statut != 'echec')
{
	// On récupère le timestamp dans les attributs
//	$timestamp = $donnees->attributes()->heure_valeurs;
	$timestamp = $donnees->attributes()->heure_contact;
	$insert_string_fields .= ", timestamp";
	$insert_string_values .= ", ".$timestamp;

	// Boucle sur les éléments du tableau	
	foreach ($donnees as $donnee)
	{
		if ($variables[intval($donnee['donnee'])] != '')
		{
	
			$valeur = base64_decode($donnee['valeur']);
	//echo $valeur." - ";
			// Supprime le degré
			$valeur = str_replace(' dC','',$valeur);
			// supprime le pourcentage
			$valeur = str_replace('pC','',$valeur);
			// Remplace le on/off de la chaudière par une valeur numérique
			$valeur = str_replace('On','50',$valeur);
			$valeur = str_replace('Off','0',$valeur);
	
	//		$variable_bdd = $variables[intval($donnee['donnee'])];
	//		$valeurs["'".$variable_bdd."'"] = $valeur;

/*
			// Récupération du timestamp renvoyé par le serveur
			if ($variables[intval($donnee['donnee'])] = 'timestamp')
				$timestamp = $valeur;				
*/	
			$insert_string_fields .= ", ".$variables[intval($donnee['donnee'])];
			$insert_string_values .= ", ".$valeur;
	
		}
		
	}
//	echo $timestamp."<br>";
	// On vérifie que le timestamp n'existe pas déjà en base
	
	$requete_verif = "SELECT timestamp FROM solisgraph WHERE timestamp = ".$timestamp;
	$result = $db->query($requete_verif);
	if (!$result)
		echo mysqli_error($db);
	else
	{
		$row = $result->fetch_assoc();
	
		if ($row['timestamp'] != $timestamp)
		{
	
			// Insertion des données dans la base mysql
		
			$requete = "INSERT INTO solisgraph
				(" . substr($insert_string_fields,1) . ")
				VALUES (" . substr($insert_string_values,1) . ")";
			
			//echo $requete."<br />";
		
			if (!$db->query($requete))
				echo mysqli_error($db);
		
		}
	}
}

?>