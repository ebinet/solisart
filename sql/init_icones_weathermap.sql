-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Client :  localhost
-- Généré le :  Mar 01 Décembre 2020 à 22:24
-- Version du serveur :  10.3.25-MariaDB-0+deb10u1
-- Version de PHP :  7.3.24-3+0~20201103.72+debian10~1.gbp945915

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `solaire`
--

-- --------------------------------------------------------

--
-- Structure de la table `wm_icones`
--

CREATE TABLE `wm_icones` (
  `id` int(4) NOT NULL,
  `groupe` varchar(30) NOT NULL,
  `description` varchar(50) NOT NULL,
  `icon` varchar(4) NOT NULL,
  `icone_jour` varchar(40) NOT NULL,
  `icone_nuit` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `wm_icones`
--

INSERT INTO `wm_icones` (`id`, `groupe`, `description`, `icon`, `icone_jour`, `icone_nuit`) VALUES
(200, 'Thunderstorm', 'thunderstorm with light rain', '11d', 'wi-day-storm-showers', 'wi-night-storm-showers'),
(201, 'Thunderstorm', 'thunderstorm with rain', '11d', 'wi-thunderstorm', 'wi-thunderstorm'),
(202, 'Thunderstorm', 'thunderstorm with heavy rain', '11d', 'wi-thunderstorm', 'wi-thunderstorm'),
(210, 'Thunderstorm', 'light thunderstorm', '11d', 'wi-day-storm-showers', 'wi-night-storm-showers'),
(211, 'Thunderstorm', 'thunderstorm', '11d', 'wi-thunderstorm', 'wi-thunderstorm'),
(212, 'Thunderstorm', 'heavy thunderstorm', '11d', 'wi-thunderstorm', 'wi-thunderstorm'),
(221, 'Thunderstorm', 'ragged thunderstorm', '11d', 'wi-thunderstorm', 'wi-thunderstorm'),
(230, 'Thunderstorm', 'thunderstorm with light drizzle', '11d', 'wi-day-storm-showers', 'wi-night-storm-showers'),
(231, 'Thunderstorm', 'thunderstorm with drizzle', '11d', 'wi-thunderstorm', 'wi-thunderstorm'),
(232, 'Thunderstorm', 'thunderstorm with heavy drizzle', '11d', 'wi-thunderstorm', 'wi-thunderstorm'),
(300, 'Drizzle', 'light intensity drizzle', '09d', 'wi-sprinkle', 'wi-sprinkle'),
(301, 'Drizzle', 'drizzle', '09d', 'wi-sprinkle', 'wi-sprinkle'),
(302, 'Drizzle', 'heavy intensity drizzle', '09d', 'wi-sprinkle', 'wi-sprinkle'),
(310, 'Drizzle', 'light intensity drizzle rain', '09d', 'wi-sprinkle', 'wi-sprinkle'),
(311, 'Drizzle', 'drizzle rain', '09d', 'wi-sprinkle', 'wi-sprinkle'),
(312, 'Drizzle', 'heavy intensity drizzle rain', '09d', 'wi-sprinkle', 'wi-sprinkle'),
(313, 'Drizzle', 'shower rain and drizzle', '09d', 'wi-sprinkle', 'wi-sprinkle'),
(314, 'Drizzle', 'heavy shower rain and drizzle', '09d', 'wi-sprinkle', 'wi-sprinkle'),
(321, 'Drizzle', 'shower drizzle', '09d', 'wi-sprinkle', 'wi-sprinkle'),
(500, 'Rain', 'light rain', '10d', 'wi-sleet', 'wi-sleet'),
(501, 'Rain', 'moderate rain', '10d', 'wi-showers', 'wi-showers'),
(502, 'Rain', 'heavy intensity rain', '10d', 'wi-rain', 'wi-rain'),
(503, 'Rain', 'very heavy rain', '10d', 'wi-rain', 'wi-rain'),
(504, 'Rain', 'extreme rain', '10d', 'wi-rain', 'wi-rain'),
(511, 'Rain', 'freezing rain', '13d', 'wi-snowflake-cold', 'wi-snowflake-cold'),
(520, 'Rain', 'light intensity shower rain', '09d', 'wi-day-sleet', 'wi-night-sleet'),
(521, 'Rain', 'shower rain', '09d', 'wi-day-rain', 'wi-night-rain'),
(522, 'Rain', 'heavy intensity shower rain', '09d', 'wi-day-rain', 'wi-night-rain'),
(531, 'Rain', 'ragged shower rain', '09d', 'wi-rain', 'wi-rain'),
(600, 'Snow', 'light snow', '13d', 'wi-snow', 'wi-snow'),
(601, 'Snow', 'Snow', '13d', 'wi-snow', 'wi-snow'),
(602, 'Snow', 'Heavy snow', '13d', 'wi-snow', 'wi-snow'),
(611, 'Snow', 'Sleet', '13d', 'wi-sleet', 'wi-sleet'),
(612, 'Snow', 'Light shower sleet', '13d', 'wi-sleet', 'wi-sleet'),
(613, 'Snow', 'Shower sleet', '13d', 'wi-sleet', 'wi-sleet'),
(615, 'Snow', 'Light rain and snow', '13d', 'wi-rain-mix', 'wi-rain-mix'),
(616, 'Snow', 'Rain and snow', '13d', 'wi-rain-mix', 'wi-rain-mix'),
(620, 'Snow', 'Light shower snow', '13d', 'wi-day-snow-wind', 'wi-night-snow-wind'),
(621, 'Snow', 'Shower snow', '13d', 'wi-day-snow-wind', 'wi-night-snow-wind'),
(622, 'Snow', 'Heavy shower snow', '13d', 'wi-snow', 'wi-snow'),
(701, 'Mist', 'mist', '50d', 'wi-day-fog', 'wi-night-fog'),
(711, 'Smoke', 'Smoke', '50d', 'wi-smoke', 'wi-smoke'),
(721, 'Haze', 'Haze', '50d', 'wi-day-fog', 'wi-night-fog'),
(731, 'Dust', 'sand/ dust whirls', '50d', 'wi-dust', 'wi-dust'),
(741, 'Fog', 'fog', '50d', 'wi-fog', 'wi-fog'),
(751, 'Sand', 'sand', '50d', 'wi-sandstorm', 'wi-sandstorm'),
(761, 'Dust', 'dust', '50d', 'wi-dust', 'wi-dust'),
(762, 'Ash', 'volcanic ash', '50d', 'wi-volcano', 'wi-volcano'),
(771, 'Squall', 'squalls', '50d', 'wi-windy', 'wi-windy'),
(781, 'Tornado', 'tornado', '50d', 'wi-tornado', 'wi-tornado'),
(800, 'Clear', 'clear sky', '01d', 'wi-day-sunny', 'wi-night-sunny'),
(801, 'Clouds', 'few clouds: 11-25%', '02d', 'wi-day-sunny-overcast', 'wi-night-sunny-overcast'),
(802, 'Clouds', 'scattered clouds: 25-50%', '03d', 'wi-day-cloudy', 'wi-night-cloudy'),
(803, 'Clouds', 'broken clouds: 51-84%', '04d', 'wi-cloud', 'wi-cloud'),
(804, 'Clouds', 'overcast clouds: 85-100%', '04d', 'wi-cloudy', 'wi-cloudy');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `wm_icones`
--
ALTER TABLE `wm_icones`
  ADD PRIMARY KEY (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
