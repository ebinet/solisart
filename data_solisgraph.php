<?php

include("includes/config.inc.php"); 

if ($_GET['type'])
	$type=$_GET['type'];
else
	$type="duree";

if ($type != 'duree' and $type != 'intervale')
	die("Erreur de type : ".$type);	

// Type durée
if ($type=='duree')
{
	if ($_GET['nb_heures'])
		$nb_heures=$_GET['nb_heures'];
	else
		$nb_heures=48;

	if (!is_numeric($nb_heures))
		die("Erreur de format du nombre d'heures");	

	$debut = strtotime("-$nb_heures hour", time());	
	
	$condition_requete = " WHERE timestamp > ".$debut; 
}

// Type intervale
if ($type=='intervale')
{
	if ($_GET['debut'])
		$debut=$_GET['debut'];
	if (!is_numeric($debut))
		die("Erreur de format du timestamp de début : ".$debut);	

	if ($_GET['fin'])
		$fin=$_GET['fin'];
	if (!is_numeric($fin))
		die("Erreur de format du timestamp de fin : ".$fin);	
	
	$condition_requete = " WHERE timestamp >= ".$debut." AND timestamp <= ".$fin; 
}


$db = new mysqli($db_host, $db_user, $db_pwd, $db_name);

	$requete = "SELECT timestamp, t1, t2, t3, t4, t7, t8, t9, t11, tcons1, v3v, c1, c4, c5, chaudiere FROM solisgraph ".$condition_requete;
	$results = $db->query($requete);

	header("Content-Type: application/csv-tab-delimited-table"); 
	header("Content-disposition: filename=data.csv"); 
	
	//En-têtes de colonnes
	echo "timestamp,T1 Capt Chaud 1,T2 Capt Froid,T3 Bal Solaire,T4 Bal Appoint,T7 Coll Froid,T8 Coll Chaud,T9 Exterieure,T11 Maison,T Consigne,Vanne 3 voies,C1 ZONE 1,C4 BAL Appoint,C5 BAL Solaire,Chaudière\n";

	while($row = $results->fetch_assoc())
	{
	    $timestamp3 = $row['timestamp']*1000; // millisecondes
	    echo($timestamp3.",");
	    echo($row['t1'].",");
	    echo($row['t2'].",");
	    echo($row['t3'].",");
	    echo($row['t4'].",");
	    echo($row['t7'].",");
	    echo($row['t8'].",");
	    echo($row['t9'].",");
	    echo($row['t11'].",");
	    echo($row['tcons1'].",");
	    echo($row['v3v'].",");
	    echo($row['c1'].",");
	    echo($row['c4'].",");
	    echo($row['c5'].",");
	    echo($row['chaudiere']."\n");
	}
	
/*
	{
		$tableau[] = array(
			'x' => $row('timestamp'),
			'y' => $row('conso')
		);
	}
*/
// var_dump($tableau);
//	echo(json_encode($tableau));


?>