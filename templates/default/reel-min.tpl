<!doctype html>
<html lang="fr">
<head>
	<meta http-equiv="content-type" content="text/html" charset="utf-8" />
	<title>SolisArt - {$numero_installation}</title>
	<script src="js/jquery-3.5.1.min.js"></script>
	<script src="js/lecture_valeurs-min.js"></script>
	<script src="js/commun-donnees.js"></script>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="css/minimal.css" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>
	<br/>
	<table>
		<th>
			<a href='./index.php'>
					<svg class="svg-icon" viewBox="0 0 20 20">
					<path d="M18.121,9.88l-7.832-7.836c-0.155-0.158-0.428-0.155-0.584,0L1.842,9.913c-0.262,0.263-0.073,0.705,0.292,0.705h2.069v7.042c0,0.227,0.187,0.414,0.414,0.414h3.725c0.228,0,0.414-0.188,0.414-0.414v-3.313h2.483v3.313c0,0.227,0.187,0.414,0.413,0.414h3.726c0.229,0,0.414-0.188,0.414-0.414v-7.042h2.068h0.004C18.331,10.617,18.389,10.146,18.121,9.88 M14.963,17.245h-2.896v-3.313c0-0.229-0.186-0.415-0.414-0.415H8.342c-0.228,0-0.414,0.187-0.414,0.415v3.313H5.032v-6.628h9.931V17.245z M3.133,9.79l6.864-6.868l6.867,6.868H3.133z"></path>
					</svg>
			</a>
			<a href='./solisgraph.php'>
					<svg class="svg-icon" viewBox="0 0 20 20">
					<path d="M17.431,2.156h-3.715c-0.228,0-0.413,0.186-0.413,0.413v6.973h-2.89V6.687c0-0.229-0.186-0.413-0.413-0.413H6.285c-0.228,0-0.413,0.184-0.413,0.413v6.388H2.569c-0.227,0-0.413,0.187-0.413,0.413v3.942c0,0.228,0.186,0.413,0.413,0.413h14.862c0.228,0,0.413-0.186,0.413-0.413V2.569C17.844,2.342,17.658,2.156,17.431,2.156 M5.872,17.019h-2.89v-3.117h2.89V17.019zM9.587,17.019h-2.89V7.1h2.89V17.019z M13.303,17.019h-2.89v-6.651h2.89V17.019z M17.019,17.019h-2.891V2.982h2.891V17.019z"></path>
					</svg>
			</a>
			<a href='./reel.php'>
					<svg class="svg-icon" viewBox="0 0 20 20">
					<path d="M12.546,4.6h-5.2C4.398,4.6,2,7.022,2,10c0,2.978,2.398,5.4,5.346,5.4h5.2C15.552,15.4,18,12.978,18,10C18,7.022,15.552,4.6,12.546,4.6 M12.546,14.6h-5.2C4.838,14.6,2.8,12.536,2.8,10s2.038-4.6,4.546-4.6h5.2c2.522,0,4.654,2.106,4.654,4.6S15.068,14.6,12.546,14.6 M12.562,6.2C10.488,6.2,8.8,7.904,8.8,10c0,2.096,1.688,3.8,3.763,3.8c2.115,0,3.838-1.706,3.838-3.8C16.4,7.904,14.678,6.2,12.562,6.2 M12.562,13C10.93,13,9.6,11.654,9.6,10c0-1.654,1.33-3,2.962-3C14.21,7,15.6,8.374,15.6,10S14.208,13,12.562,13"></path>
					</svg>
			</a>
		</th>
	</table>
	<br/>
	<span id="data">Données Solisart : <span id="contact-installation">En attente...</span></span><br><br>
	<table width=100%>
		<tr>
			<th colspan="3">Temp&eacute;ratures</th>
		</tr>
		<tr>
			<td id="" width=33%><b>Intérieur</b><br/><span id="temp-valeur-11">00,0°C</span></td>
			<td id="center" width=33%><b>Extérieur</b><br/><span id="temp-valeur-9">00,0°C</span></td>
			<td id="" width=33%><b>ECS</b><br/><span id="temp-valeur-3">00,0°C</span>&nbsp;&nbsp;<span id="temp-valeur-4">00,0°C</span></td>
		</tr>
	</table>
	<br/>
	<table width=100%>
		<tr>
			<th colspan="4">Circulateurs</th>
		</tr>
		<tr>
			<td id="circulateurs" width=25%>
				<b>Maison</b><br/><span id="circ-valeur-5"><img src="image/circle-grey.png" /></span>
				<span id="collecteur"><br><span id="temp-valeur-8"></span>&nbsp;&nbsp;<span id="temp-valeur-7"></span></span>
			</td>
			<td id="circulateurs" width=25%>
				<b>ECS Bas</b><br/>
				<span id="circ-valeur-2"><img src="image/circle-grey.png" /></span>
			</td>
			<td id="circulateurs" width=25%>
				<b>ECS Haut</b><br/>
				<span id="circ-valeur-1"><img src="image/circle-grey.png" /></span>
			</td>
			<td id="circulateurs" width=25%>
				<b>Chaudière</b><br/><span id="check-chaudiere-1-label"><img src="image/circle-grey.png" /></span></td>    
		</tr>
	</table>
	<br/>
	<table width=100%>
		<tr>
			<th colspan="3">Capteurs</th>
		</tr>
		<tr>
			<td id="" width=33%>
				<b>Chaud</b><br/><span id="temp-valeur-1">00,0°C</span>
			</td>
			<td id="center" width=33%>
				<b>Froid</b><br/><span id="temp-valeur-2">00,0°C</span>
			</td>
			<td id="" width=33%>
				<b>V3V</b><br/><span id="v3v-valeur-1">--%</span>
			</td>
		</tr>
	</table>
	<br/>
	
	<form action="" method="post" enctype="multipart/chauffage-data" id="chauffage" name="chauffage"> 
	<table width=100%>
		<tr>
			<th colspan="4">Chauffage</th>
		</tr>
		<tr>
			<td>
				<select class="select-css" id="consigne-chauffage" name="consigne-chauffage" onchange="modif_chauffage()">
				{html_options options=$libelle_consigne}
				</select>								
				<br><span id="temoin_chauffage" style="border: none; background: none;"><img src="image/bullet_black.png"></span>
			</td>
			<td>
				<input class="input-css" type="text" inputmode="decimal" size="2" id="consigne-hp-chauffage" name="consigne-hp-chauffage" value="00,0" onchange="modif_hp_chauffage()">
				<br><span id="temoin_hp_chauffage" style="border: none; background: none;"><img src="image/bullet_black.png"></span>
			</td>
			<td>
				<input class="input-css" type="text" inputmode="decimal" size="2" id="consigne-hc-chauffage" name="consigne-hc-chauffage" value="00,0" onchange="modif_hc_chauffage()">
				<br><span id="temoin_hc_chauffage" style="border: none; background: none;"><img src="image/bullet_black.png"></span>
			</td>
			<td>
            <input class="button-css" id="bouton-chauffage" type = "button" value="OK" onclick="modifier_chauffage()">
				<br><span id="temoin_retour_chauffage" style="border: none; background: none;"><img src="image/bullet_green.png"></span>
			</td>
		</tr>
	</table>
	<br/>
	</form>

	<form action="" method="post" enctype="multipart/ecs-data" id="ecs" name="ecs"> 
	<table width=100%>
		<tr>
			<th colspan="4">Eau chaude</th>
		</tr>
		<tr>
			<td>
				<select class="select-css" id="consigne-ecs" name="consigne-ecs" onchange="modif_ecs()">
				{html_options options=$libelle_consigne}
				</select>
				<br><span id="temoin_ecs" style="border: none; background: none;"><img src="image/bullet_black.png"></span>
			</td>
			<td>
				<input class="input-css" type="text" inputmode="decimal" size="2" id="consigne-hp-ecs" name="consigne-hp-ecs" value="00,0" onchange="modif_hp_ecs()">
				<br><span id="temoin_hp_ecs" style="border: none; background: none;"><img src="image/bullet_black.png"></span>
			</td>
			<td>
				<input class="input-css" type="text" inputmode="decimal" size="2" id="consigne-hc-ecs" name="consigne-hc-ecs" value="00,0" onchange="modif_hc_ecs()">
				<br><span id="temoin_hc_ecs" style="border: none; background: none;"><img src="image/bullet_black.png"></span>
			</td>
			<td>
				<input class="button-css" id="bouton-chauffage" type = "button" value="OK" onclick="modifier_ecs()">
				<br><span id="temoin_retour_ecs" style="border: none; background: none;"><img src="image/bullet_green.png"></span>
			</td>
	</table>
	<br/>
	<input class="button-css" style="width: 7em;" type="button" value="Rafraîchir" onclick="rafraichir_valeurs();">
	</form>
	<br/>
	<br/>
</body>
