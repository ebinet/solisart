<!doctype html>
<html lang="fr">
	<head>
		<meta http-equiv="content-type" content="text/html" charset="utf-8" />
		<title>SolisArt - Installation {$numero_installation}</title>
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
		<script src="js/lecture_valeurs.js"></script>
		<script src="js/commun-donnees.js"></script>
		<link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css" />
		<link rel="stylesheet" type="text/css" href="css/commun.css" />
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
		<link rel="icon" href="/favicon.ico" type="image/x-icon">
	</head>
<body>
<table><tr><td id="contact-installation">Date et heure de contact</td><td id="message-systeme"></td></tr></table>
<div id="onglets">
	<div id="onglet-maintenance">
		<table>
			<tr>
				<td>
					<fieldset class="ui-widget-content">
						<legend class="ui-widget-header ui-state-default" style="border: none; background: none;">Temp&eacute;ratures</legend>
						<table>
							<tr>
								<td id="temp-libelle-1">T1 Capt Chaud 1</td>
								<td id="temp-valeur-1">: 00,0 °C</td>
								<td>&nbsp;</td>
								<td id="temp-libelle-9">T9 Exterieure</td>
								<td id="temp-valeur-9">: 00,0 °C</td>
							</tr>
							<tr>
								<td id="temp-libelle-2">T2 Capt Froid</td>
								<td id="temp-valeur-2">: 00,0 °C</td>
								<td>&nbsp;</td>
								<td id="temp-libelle-10-non-unilise"></td>
								<td id="temp-valeur-10-non-unilise"></td>
							</tr>
							<tr>
								<td id="temp-libelle-3">T3 Bal Solaire</td>
								<td id="temp-valeur-3">: 00,0 °C</td>
								<td>&nbsp;</td>
								<td id="temp-libelle-11">T11 Maison</td>
								<td id="temp-valeur-11">: 00,0 °C</td>
							</tr>
							<tr>
								<td id="temp-libelle-4">T4 Bal Appoint</td>
								<td id="temp-valeur-4">: 00,0 °C</td>
								<td>&nbsp;</td>
								<td id="temp-libelle-12-non-unilise"></td>
								<td id="temp-valeur-12-non-unilise"></td>
							</tr>
							<tr>
								<td id="temp-libelle-5-non-unilise"></td>
								<td id="temp-valeur-5-non-unilise"></td>
								<td>&nbsp;</td>
								<td id="temp-libelle-13-non-unilise"></td>
								<td id="temp-valeur-13-non-unilise"></td>
							</tr>
							<tr>
								<td id="temp-libelle-6-non-unilise"></td>
								<td id="temp-valeur-6-non-unilise"></td>
								<td>&nbsp;</td>
								<td id="temp-libelle-14-non-unilise"></td>
								<td id="temp-valeur-14-non-unilise"></td>
							</tr>
							<tr>
								<td id="temp-libelle-7">T7 Coll Froid</td>
								<td id="temp-valeur-7">: 00,0 °C</td>
								<td>&nbsp;</td>
								<td id="temp-libelle-15-non-unilise"></td>
								<td id="temp-valeur-15-non-unilise"></td>
							</tr>
							<tr>
								<td id="temp-libelle-8">T8 Coll Chaud</td>
								<td id="temp-valeur-8">: 00,0 °C</td>
								<td>&nbsp;</td>
								<td id="temp-libelle-16-non-unilise"></td>
								<td id="temp-valeur-16-non-unilise"></td>
							</tr>

						</table>
					</fieldset>
				</td>
				<td>
					<fieldset class="ui-widget-content">
						<legend class="ui-widget-header ui-state-default" style="border: none; background: none;">Chaudi&egrave;re</legend>
						<label id="check-chaudiere-1-label" for="check-chaudiere-1"><img src="image/led_grise.png" />&nbsp;&nbsp;En attente...</label>
					</fieldset>
					<form action="" method="post" enctype="multipart/chauffage-data" id="chauffage" name="chauffage"> 
						<fieldset class="ui-widget-content">
							<legend class="ui-widget-header ui-state-default" style="border: none; background: none;">Chauffage</legend>
								<select id="consigne-chauffage" name="consigne-chauffage" onchange="modif_chauffage()">
								{html_options options=$libelle_consigne}
								</select>
								<input type="text" size="2" id="consigne-hp-chauffage" name="consigne-hp-chauffage" value="00,0" onchange="modif_hp_chauffage()">
								<input type="text" size="2" id="consigne-hc-chauffage" name="consigne-hc-chauffage" value="00,0" onchange="modif_hc_chauffage()">
								<input id="bouton-chauffage" type = "button" value="OK" onclick="modifier_chauffage()">
								<table><tr>
								<td id="temoin_chauffage" style="border: none; background: none;"><img src="image/bullet_black.png"></td>
								<td id="temoin_hp_chauffage" style="border: none; background: none;"><img src="image/bullet_black.png"></td>
								<td id="temoin_hc_chauffage" style="border: none; background: none;"><img src="image/bullet_black.png"></td>
								<td id="temoin_retour_chauffage" style="border: none; background: none;"><img src="image/bullet_green.png"></td>
								</tr></table>
						</fieldset>
					</form>
					<form action="" method="post" enctype="multipart/ecs-data" id="ecs" name="ecs"> 
						<fieldset class="ui-widget-content">
							<legend class="ui-widget-header ui-state-default" style="border: none; background: none;">ECS</legend>
								<select id="consigne-ecs" name="consigne-ecs" onchange="modif_ecs()">
								{html_options options=$libelle_consigne}
								</select>
								<input type="text" size="2" id="consigne-hp-ecs" name="consigne-hp-ecs" value="00,0" onchange="modif_hp_ecs()">
								<input type="text" size="2" id="consigne-hc-ecs" name="consigne-hc-ecs" value="00,0" onchange="modif_hc_ecs()">
								<input id="bouton-ecs" type = "button" value="OK" onclick="modifier_ecs()">
								<table><tr>
								<td id="temoin_ecs" style="border: none; background: none;"><img src="image/bullet_black.png"></td>
								<td id="temoin_hp_ecs" style="border: none; background: none;"><img src="image/bullet_black.png"></td>
								<td id="temoin_hc_ecs" style="border: none; background: none;"><img src="image/bullet_black.png"></td>
								<td id="temoin_retour_ecs" style="border: none; background: none;"><img src="image/bullet_green.png"></td>
								</tr></table>
						</fieldset>
					</form>
				</td>
			</tr>
			<tr>
				<td>
					<fieldset class="ui-widget-content">
						<legend class="ui-widget-header ui-state-default" style="border: none; background: none;">Circulateurs et V3V</legend>
						<table>
							<tr>
								<td id="circ-libelle-1">C4 BAL Appoint</td>
								<td id="circ-valeur-1"><img src="image/led_grise.png" /></td>
							</tr>
							<tr>
								<td id="circ-libelle-2">C5 BAL Solaire</td>
								<td id="circ-valeur-2"><img src="image/led_grise.png" /></td>
							</tr>
							<tr>
								<td id="circ-libelle-3"></td>
							</tr>
							<tr>
								<td id="circ-libelle-5">C1 ZONE 1</td>
								<td id="circ-valeur-5"><img src="image/led_grise.png" /></td>
							</tr>
							<tr>
								<td>&nbsp</td>
							</tr>

							<tr>
								<td colspan="2">Position de la V3V appoint :</td>
								<td id="v3v-valeur-1">0 %</td>
							</tr>
							<tr>
								<td colspan="3"><div class="oblique">0% : vers les capteurs</div></td>
							</tr>
						</table>
					</fieldset>
				</td>
				<td>&nbsp</td>
				<td>&nbsp</td>
			</tr>
		</table>
		<br />
		&nbsp;&nbsp;<a href="/solisgraph" >Solisgraph</a><br>
		&nbsp;&nbsp;<a href="/">Accueil</a><br>
		<br>
<!--		<input type="button" value="Rafraîchir" onclick="javascript:location.href='reel.php'">-->
		<input type="button" value="Rafraîchir" onclick="rafraichir_valeurs();">
	</div>
</div>
</body>