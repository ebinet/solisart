<!doctype html>
<html lang="fr">
	<head>
		<meta http-equiv="content-type" content="text/html" charset="utf-8" />
		<title>SolisArt - Installation {$numero_installation}</title>
		<link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css" />
		<link rel="stylesheet" type="text/css" href="css/commun.css" />
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
		<link rel="icon" href="/favicon.ico" type="image/x-icon">
	</head>
<body>
&nbsp&nbsp&nbsp{$valeurs[985]}
<div id="onglets">
	<div id="onglet-maintenance">
		<table>
			<tr>
				<td>
					<fieldset class="ui-widget-content">
						<legend class="ui-widget-header ui-state-default" style="border: none; background: none;">Temp&eacute;ratures</legend>
						<table>
							<tr>
								<td id="temp-libelle-1">T1 Capt Chaud 1</td>
								<td id="temp-valeur-1">: {$valeurs[584]}</td>
								<td>&nbsp;</td>
								<td id="temp-libelle-9">T9 Exterieure</td>
								<td id="temp-valeur-9">: {$valeurs[592]}</td>
							</tr>
							<tr>
								<td id="temp-libelle-2">T2 Capt Froid</td>
								<td id="temp-valeur-2">: {$valeurs[585]}</td>
								<td>&nbsp;</td>
								<td id="temp-libelle-10"></td>
								<td id="temp-valeur-10"></td>
							</tr>
							<tr>
								<td id="temp-libelle-3">T3 Bal Solaire</td>
								<td id="temp-valeur-3">: {$valeurs[586]}</td>
								<td>&nbsp;</td>
								<td id="temp-libelle-11">T11 Maison</td>
								<td id="temp-valeur-11">: {$valeurs[594]}</td>
							</tr>
							<tr>
								<td id="temp-libelle-4">T4 Bal Appoint</td>
								<td id="temp-valeur-4">: {$valeurs[587]}</td>
								<td>&nbsp;</td>
								<td id="temp-libelle-12"></td>
								<td id="temp-valeur-12"></td>
							</tr>
							<tr>
								<td id="temp-libelle-5"></td>
								<td id="temp-valeur-5"></td>
								<td>&nbsp;</td>
								<td id="temp-libelle-13"></td>
								<td id="temp-valeur-13"></td>
							</tr>
							<tr>
								<td id="temp-libelle-6"></td>
								<td id="temp-valeur-6"></td>
								<td>&nbsp;</td>
								<td id="temp-libelle-14"></td>
								<td id="temp-valeur-14"></td>
							</tr>
							<tr>
								<td id="temp-libelle-7">T7 Coll Froid</td>
								<td id="temp-valeur-7">: {$valeurs[590]}</td>
								<td>&nbsp;</td>
								<td id="temp-libelle-15"></td>
								<td id="temp-valeur-15"></td>
							</tr>
							<tr>
								<td id="temp-libelle-8">T8 Coll Chaud</td>
								<td id="temp-valeur-8">: {$valeurs[591]}</td>
								<td>&nbsp;</td>
								<td id="temp-libelle-16"></td>
								<td id="temp-valeur-16"></td>
							</tr>

						</table>
					</fieldset>
				</td>
				<td>
					<fieldset class="ui-widget-content">
						<legend class="ui-widget-header ui-state-default" style="border: none; background: none;">Chaudi&egrave;re</legend>
						<label id="check-chaudiere-1-label" for="check-chaudiere-1">{$valeurs[606]}</label>
					</fieldset>
					<form action="" method="post" enctype="multipart/chauffage-data" id="chauffage" name="chauffage"> 
						<fieldset class="ui-widget-content">
							<legend class="ui-widget-header ui-state-default" style="border: none; background: none;">Chauffage</legend>
								<select name="consigne-chauffage" onchange="chauffage.submit()">
								{html_options options=$libelle_consigne selected=$valeurs[150]}
								</select>
								<input type="text" size="2" id="consigne-hp-chauffage" name="consigne-hp-chauffage" value={$valeurs[157]}>
								<input type="text" size="2" id="consigne-hc-chauffage" name="consigne-hc-chauffage" value={$valeurs[164]}>
								<input id="bouton-chauffage" type = "button" value="OK" onclick="modifier_chauffage()">
						</fieldset>
					</form>
					<form action="" method="post" enctype="multipart/ecs-data" id="ecs" name="ecs"> 
						<fieldset class="ui-widget-content">
							<legend class="ui-widget-header ui-state-default" style="border: none; background: none;">ECS</legend>
								<select name="consigne-ecs" onchange="ecs.submit()">
								{html_options options=$libelle_consigne selected=$valeurs[156]}
								</select>
								<input type="text" size="2" id="consigne-hp-ecs" name="consigne-hp-ecs" value={$valeurs[163]}>
								<input type="text" size="2" id="consigne-hc-ecs" name="consigne-hc-ecs" value={$valeurs[170]}>
								<input id="bouton-ecs" type = "button" value="OK" onclick="modifier_ecs()">
						</fieldset>
					</form>
				</td>
			</tr>
			<tr>
				<td>
					<fieldset class="ui-widget-content">
						<legend class="ui-widget-header ui-state-default" style="border: none; background: none;">Circulateurs et V3V</legend>
						<table>
							<tr>
								<td id="circ-libelle-1">C4 BAL Appoint</td>
 							<td id="circ-valeur-1">{$valeurs[614]}</td>
							</tr>
							<tr>
								<td id="circ-libelle-2">C5 BAL Solaire</td>
								<td id="circ-valeur-2">{$valeurs[615]}</td>
							</tr>
							<tr>
								<td id="circ-libelle-3"></td>
							</tr>
							<tr>
								<td id="circ-libelle-5">C1 ZONE 1</td>
								<td id="circ-valeur-5">{$valeurs[618]}</td>
							</tr>
							<tr>
								<td id="circ-libelle-6">&nbsp</td>
							</tr>

							<tr>
								<td colspan="2">Position de la V3V appoint :</td>
								<td id="v3v-valeur-1">{$valeurs[628]} %</td>
							</tr>
							<tr>
								<td colspan="3"><div class="oblique">0% : vers les capteurs</div></td>
							</tr>
						</table>
					</fieldset>
				</td>
				<td>&nbsp</td>
				<td>&nbsp</td>
			</tr>
		</table>
		<br />
		&nbsp;&nbsp;<a href="/solisgraph" >Solisgraph</a><br>
		&nbsp;&nbsp;<a href="/">Dernières données</a><br>
		<br>
		<input type="button" value="Rafraîchir" onclick="javascript:location.href='direct.php'">
	</div>
</div>
</body>