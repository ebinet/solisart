<!doctype html>
<html lang="fr">
	<head>
		<meta http-equiv="content-type" content="text/html" charset="utf-8" />
		<title>SolisArt - Installation {$numero_installation}</title>
		<link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css" />
		<link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker-2.4.1.css" />
		<link rel="stylesheet" type="text/css" href="css/commun.css" />
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
		<link rel="icon" href="/favicon.ico" type="image/x-icon">
	</head>
<body>
&nbsp&nbsp&nbsp{$valeurs[985]}
<div id="onglets">
	<div id="onglet-maintenance">
		<table>
			<tr>
				<td>
					<fieldset class="ui-widget-content">
						<legend class="ui-widget-header ui-state-default" style="border: none; background: none;">Temp&eacute;ratures</legend>
						<table>
							<tr>
								<td id="temp-libelle-1">T1 Capt Chaud 1</td>
								<td id="temp-valeur-1">: {$valeurs['t1']}</td>
								<td>&nbsp;</td>
								<td id="temp-libelle-9">T9 Extérieure</td>
								<td id="temp-valeur-9">: {$valeurs['t9']}</td>
							</tr>
							<tr>
								<td id="temp-libelle-2">T2 Capt Froid</td>
								<td id="temp-valeur-2">: {$valeurs['t2']}</td>
								<td>&nbsp;</td>
								<td id="temp-libelle-10"></td>
								<td id="temp-valeur-10"></td>
							</tr>
							<tr>
								<td id="temp-libelle-3">T3 Bal Solaire</td>
								<td id="temp-valeur-3">: {$valeurs['t3']}</td>
								<td>&nbsp;</td>
								<td id="temp-libelle-11">T11 Maison</td>
								<td id="temp-valeur-11">: {$valeurs['t11']}</td>
							</tr>
							<tr>
								<td id="temp-libelle-4">T4 Bal Appoint</td>
								<td id="temp-valeur-4">: {$valeurs['t4']}</td>
								<td>&nbsp;</td>
								<td id="temp-libelle-12"></td>
								<td id="temp-valeur-12"></td>
							</tr>
							<tr>
								<td id="temp-libelle-5"></td>
								<td id="temp-valeur-5"></td>
								<td>&nbsp;</td>
								<td id="temp-libelle-13"></td>
								<td id="temp-valeur-13"></td>
							</tr>
							<tr>
								<td id="temp-libelle-6"></td>
								<td id="temp-valeur-6"></td>
								<td>&nbsp;</td>
								<td id="temp-libelle-14"></td>
								<td id="temp-valeur-14"></td>
							</tr>
							<tr>
								<td id="temp-libelle-7">T7 Coll Froid</td>
								<td id="temp-valeur-7">: {$valeurs['t7']}</td>
								<td>&nbsp;</td>
								<td id="temp-libelle-15"></td>
								<td id="temp-valeur-15"></td>
							</tr>
							<tr>
								<td id="temp-libelle-8">T8 Coll Chaud</td>
								<td id="temp-valeur-8">: {$valeurs['t8']}</td>
								<td>&nbsp;</td>
								<td id="temp-libelle-16"></td>
								<td id="temp-valeur-16"></td>
							</tr>

						</table>
					</fieldset>
				</td>
				<td>
					<fieldset class="ui-widget-content">
						<legend class="ui-widget-header ui-state-default" style="border: none; background: none;">Chaudi&egrave;re</legend>
						<label id="check-chaudiere-1-label" for="check-chaudiere-1">{$valeurs['chaudiere']}</label>
					</fieldset>
				</td>
			</tr>
			<tr>
				<td>
					<fieldset class="ui-widget-content">
						<legend class="ui-widget-header ui-state-default" style="border: none; background: none;">Circulateurs et V3V</legend>
						<table>
							<tr>
								<td id="circ-libelle-1">C4 BAL Appoint</td>
								<td id="circ-valeur-1">{$valeurs['c4']}</td>
							</tr>
							<tr>
								<td id="circ-libelle-2">C5 BAL Solaire</td>
								<td id="circ-valeur-2">{$valeurs['c5']}</td>
							</tr>
							<tr>
								<td id="circ-libelle-3"></td>
								<td id="circ-valeur-3"></td>
							</tr>
							<tr>
								<td id="circ-libelle-4">C1 ZONE 1</td>
								<td id="circ-valeur-4">{$valeurs['c1']}</td>
							</tr>
							<tr>
								<td id="circ-libelle-4">&nbsp</td>
							</tr>

							<tr>
								<td colspan="2">Position de la V3V appoint :</td>
								<td id="v3v-valeur-1">{$valeurs['v3v']} %</td>
							</tr>
							<tr>
								<td colspan="3"><div class="oblique">0% : vers les capteurs</div></td>
							</tr>
						</table>
					</fieldset>
				</td>
			</tr>
		</table>
		<table>
			<tr><td>&nbsp;Maintenant : </td><td>{$valeurs['maintenant'] }</td></tr>
			<tr><td>&nbsp;Données : </td><td>{$valeurs['timestamp'] }</td></tr>
		</table>
		&nbsp;&nbsp;<font color="red">{$valeurs['alerte'] }</font><br>
		&nbsp;&nbsp;<a href="/solisgraph" >Solisgraph</a><br>
		&nbsp;&nbsp;<a href="/reel.php" >Temps réel</a>
	</div>
</div>
</body>