<!doctype html>
<html lang="fr">
<head>
	<meta http-equiv="content-type" content="text/html" charset="utf-8" />
	<title>SolisArt - {$numero_installation}</title>
	
	<link rel="stylesheet" type="text/css" href="css/minimal.css" />
	<link rel="stylesheet" type="text/css" href="css/DateTimePicker.css" />

	<script src="js/jquery-3.5.1.min.js"></script> 
	<script src="highcharts/code/highcharts.js"></script>
	<script src="highcharts/code/modules/boost.js"></script>
	<script src="highcharts/code/modules/exporting.js"></script>

	<script src="highcharts/code/modules/series-label.js"></script>
	<script src="highcharts/code/modules/data.js"></script>
	<script src="highcharts/code/modules/export-data.js"></script>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.13/moment-timezone-with-data-2012-2022.min.js"></script>

<!-- ------------------------------------------------------------>  
<!-- LE SCRIPT HIGHCHARTS SCRIPT.JS EST TOUT EN BAS DE LA PAGE -->
<!-- ------------------------------------------------------------>  
        
</head>

<body>
	<br/>
	<table>
		<th id="hautdepage">
			<span class="fleche" style="float: left;"><a href="#container"><img src="image/flechebas.svg" height="30px" style="margin: 10px 0 0 5px; float: right;" ></a></span>
			<a href='index.php'>
				<svg class="svg-icon" viewBox="0 0 20 20">
				<path d="M18.121,9.88l-7.832-7.836c-0.155-0.158-0.428-0.155-0.584,0L1.842,9.913c-0.262,0.263-0.073,0.705,0.292,0.705h2.069v7.042c0,0.227,0.187,0.414,0.414,0.414h3.725c0.228,0,0.414-0.188,0.414-0.414v-3.313h2.483v3.313c0,0.227,0.187,0.414,0.413,0.414h3.726c0.229,0,0.414-0.188,0.414-0.414v-7.042h2.068h0.004C18.331,10.617,18.389,10.146,18.121,9.88 M14.963,17.245h-2.896v-3.313c0-0.229-0.186-0.415-0.414-0.415H8.342c-0.228,0-0.414,0.187-0.414,0.415v3.313H5.032v-6.628h9.931V17.245z M3.133,9.79l6.864-6.868l6.867,6.868H3.133z"></path>
				</svg>
			</a>
			<a href='#'>                 
				<svg class="svg-icon" viewBox="0 0 20 20">
				<path d="M17.431,2.156h-3.715c-0.228,0-0.413,0.186-0.413,0.413v6.973h-2.89V6.687c0-0.229-0.186-0.413-0.413-0.413H6.285c-0.228,0-0.413,0.184-0.413,0.413v6.388H2.569c-0.227,0-0.413,0.187-0.413,0.413v3.942c0,0.228,0.186,0.413,0.413,0.413h14.862c0.228,0,0.413-0.186,0.413-0.413V2.569C17.844,2.342,17.658,2.156,17.431,2.156 M5.872,17.019h-2.89v-3.117h2.89V17.019zM9.587,17.019h-2.89V7.1h2.89V17.019z M13.303,17.019h-2.89v-6.651h2.89V17.019z M17.019,17.019h-2.891V2.982h2.891V17.019z"></path>
				</svg>
			</a>      
			<a href='reel.php'>
				<svg class="svg-icon" viewBox="0 0 20 20">
				<path d="M12.546,4.6h-5.2C4.398,4.6,2,7.022,2,10c0,2.978,2.398,5.4,5.346,5.4h5.2C15.552,15.4,18,12.978,18,10C18,7.022,15.552,4.6,12.546,4.6 M12.546,14.6h-5.2C4.838,14.6,2.8,12.536,2.8,10s2.038-4.6,4.546-4.6h5.2c2.522,0,4.654,2.106,4.654,4.6S15.068,14.6,12.546,14.6 M12.562,6.2C10.488,6.2,8.8,7.904,8.8,10c0,2.096,1.688,3.8,3.763,3.8c2.115,0,3.838-1.706,3.838-3.8C16.4,7.904,14.678,6.2,12.562,6.2 M12.562,13C10.93,13,9.6,11.654,9.6,10c0-1.654,1.33-3,2.962-3C14.21,7,15.6,8.374,15.6,10S14.208,13,12.562,13"></path>
				</svg>
         </a>      
		</th> 
	</table>  

     <div style="margin:10px 0 10px 0" id="data">Données Solisart : <span id="derniere_donnee"></div></span>&nbsp;<span id="libelle_alerte"></span>

	<table class="graphique">
		<tr>
			<td>
				<div id="container" class="highchart-container"></div>
			</td>
		</tr>
	</table>
	

	<div class="wrapper">
		<div class="table2 grille-controles">
			<div class="td2" width=10% style="vertical-align: top; text-align: center;" rowspan="2">
				<input class="button-css" type="button" value="OK" onclick="updateChart(); UpdateTitle();"><br/><br>
				<input class="button-css" type="button" value="RAZ" onclick="init_saisie(); updateChart(); UpdateTitle();"><br>
				<span class="fleche"><a href="#container"><img src="image/flechehaut.svg" height="30px" style="margin: 10px 0 0 5px;" ></a></span>
			</div>
			<div class="td2">
				<div class="puces-dates">
					<div>
						<input type="radio" id="duree" name="select_donnees" value="duree" {$select_duree}>
					</div>
					<div>
						Depuis <input type="text" inputmode="decimal" size="2" id="nb_heures" name="nb_heures" value="{$nb_heures}" onchange='$("#duree").prop("checked", true);'> heures<br/>
						<br/>
					</div>
				</div>
				<div class="puces-dates">
					<div>
						<input type="radio" id="intervalle" name="select_donnees" value="intervale" {$select_intervale}>
					</div>
					<div>
						Du <input type="date" id="date_debut" name="date_debut" onchange='$("#intervalle").prop("checked", true);'>
						<input type="time" id="heure_debut" name="heure_debut" onchange='$("#intervalle").prop("checked", true);'>
						<br/><br/>                   
						au <input type="date" id="date_fin" name="date_fin" data-field="datetime" onchange='$("#intervalle").prop("checked", true);'>
						<input type="time" id="heure_fin" name="heure_fin" data-field="datetime" onchange='$("#intervalle").prop("checked", true);'>
					</div>
				</div>
			</div>
		</div>
		<div class="table2 grille-affichage">
			<div class="td2">
				<input type="checkbox" id="check_t1" name="check_t1" onclick="clicbox(this.id)" {$liste_courbes['check_t1']} >
				<label for="check_t1">T1 Capt Chaud 1</label><br/>

				<input type="checkbox" id="check_t2" name="check_t2" onclick="clicbox(this.id)" {$liste_courbes['check_t2']}>
				<label for="check_t2">T2 Capt Froid</label><br/>

				<input type="checkbox" id="check_t3" name="check_t3" onclick="clicbox(this.id)" {$liste_courbes['check_t3']} >
				<label for="check_t3">T3 Bal Solaire</label><br/>

				<input type="checkbox" id="check_t4" name="check_t4" onclick="clicbox(this.id)" {$liste_courbes['check_t4']} >
				<label for="check_t4">T4 Bal Appoint</label><br/>

				<input type="checkbox" id="check_t7" name="check_t7" onclick="clicbox(this.id)" {$liste_courbes['check_t7']}>
				<label for="check_t7">T7 Coll Froid</label><br/>

				<input type="checkbox" id="check_t8" name="check_t8" onclick="clicbox(this.id)" {$liste_courbes['check_t8']}>
				<label for="check_t8">T8 Coll Chaud</label><br/>
			</div>
			<div class="td2">
				<input type="checkbox" id="check_t9" name="check_t9" onclick="clicbox(this.id)" {$liste_courbes['check_t9']} >
				<label for="check_t9">T9 Exterieure</label><br/>

				<input type="checkbox" id="check_t11" name="check_t11" onclick="clicbox(this.id)" {$liste_courbes['check_t11']} >
				<label for="check_t11">T11 Maison</label><br/>

				<input type="checkbox" id="check_tcons1" name="check_tcons1" onclick="clicbox(this.id)" {$liste_courbes['check_tcons1']} >
				<label for="check_tcons1">T Consigne</label><br/>
			</div>		
			<div class="td2">
				<input type="checkbox" id="check_c4" name="check_c4" onclick="clicbox(this.id)" {$liste_courbes['check_c4']}>
				<label for="check_c4">C4 BAL Appoint</label><br/>

				<input type="checkbox" id="check_c5" name="check_c5" onclick="clicbox(this.id)" {$liste_courbes['check_c5']}>
				<label for="check_c5">C5 BAL Solaire</label><br/>

				<input type="checkbox" id="check_c1" name="check_c1" onclick="clicbox(this.id)" {$liste_courbes['check_c1']}>
				<label for="check_c1">C1 ZONE 1</label><br/>
				
				<input type="checkbox" id="check_v3v" name="check_v3v" onclick="clicbox(this.id)" {$liste_courbes['check_v3v']}>
				<label for="check_v3v">Vanne 3 voies</label><br/>
			<br>
				<input type="checkbox" id="grouper" name="grouper" onclick="clicbox_grouper()">
				<label for="grouper">Regrouper</label><br/>
			</div>
			<div class="td2">
	
					<input type="checkbox" id="check_chaudiere" name="check_chaudiere" onclick="clicbox(this.id)" {$liste_courbes['check_chaudiere']}>
					<label for="check_chaudiere">Chaudière</label><br/>
				<br>
					<input class="button-css" type="button" value="RAZ" onclick="verifie_series()">
				</div>
		</div>
	</div>


	<br/>

<script type="text/javascript" >
	{include file='js/script.js'}
</script> 

<script type="text/javascript" >
	function UpdateTitle() { graphique.setTitle({literal}{{/literal}text: "SolisArt - {$numero_installation}"{literal}}{/literal}); };
	UpdateTitle();
</script>


</body>
    
</html>