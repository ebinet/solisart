<!doctype html>
<html lang="fr">
	<head>
		<meta http-equiv="content-type" content="text/html" charset="utf-8" />
		<title>SolisArt - {$numero_installation}</title>

		<link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css" />		
		<link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css" />
		<link rel="stylesheet" type="text/css" href="css/commun.css" />

  		<script src="js/jquery-3.5.1.min.js"></script> 
		<script src="highcharts/code/highcharts.js"></script>
		<script src="highcharts/code/modules/boost.js"></script>
		<script src="highcharts/code/modules/exporting.js"></script>
		
		<script src="highcharts/code/modules/series-label.js"></script>
		<script src="highcharts/code/modules/data.js"></script>
		<script src="highcharts/code/modules/export-data.js"></script>
	
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.13/moment-timezone-with-data-2012-2022.min.js"></script>

<!--		<link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker-2.4.1.css" />
		<script src="js/jquery.datetimepicker-2.4.1.js" charset="UTF-8"></script>
-->
<!--		<link rel="stylesheet" type="text/css" href="divers/jquery-ui-1.11.2/redmond/jquery-ui.min.css" />
		<link rel="stylesheet" type="text/css" href="divers/css/solisart/commun.1553374628.css" />
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
		<link rel="icon" href="/favicon.ico" type="image/x-icon">
		<script src="divers/js/jquery-1.11.2.min.js" charset="UTF-8"></script>
		<script src="divers/jquery-ui-1.11.2/jquery-ui.min.js" charset="UTF-8"></script>
		<script src="divers/js/latinise.min.js" charset="UTF-8"></script>
-->

<!-- ----------------------------------------------- -->  
<!-- LE SCRIPT HIGHCHARTS EST TOUT EN BAS DE LA PAGE -->
<!-- ----------------------------------------------- -->  

	</head>
<body>

<div id="container" style="width:100%; height:550px;"></div>

<div id="onglets">
	<div id="onglet-maintenance">
	<form action="{$url_defaut}" method="post" enctype="multipart/form-data" id="form" name="form"> 

<table>
<tr>
<td valign="top" style="padding-right:10px">

	<fieldset class="ui-widget-content">
	<legend class="ui-widget-header ui-state-default" style="border: none; background: none;">Affichage</legend>
	<table>
	<tr>
		<td valign="top" style="padding-right:10px">
		<div>
		  <input type="checkbox" id="check_t1" name="check_t1" onclick="clicbox(this.id)" {$liste_courbes['check_t1']} >
		  <label for="check_t1">T1 Capt Chaud 1</label>
		</div>
		<div>
		  <input type="checkbox" id="check_t2" name="check_t2" onclick="clicbox(this.id)" {$liste_courbes['check_t2']}>
		  <label for="check_t2">T2 Capt Froid</label>
		</div>
		<div>
		  <input type="checkbox" id="check_t3" name="check_t3" onclick="clicbox(this.id)" {$liste_courbes['check_t3']} >
		  <label for="check_t3">T3 Bal Solaire</label>
		</div>
		<div>
		  <input type="checkbox" id="check_t4" name="check_t4" onclick="clicbox(this.id)" {$liste_courbes['check_t4']} >
		  <label for="check_t4">T4 Bal Appoint</label>
		</div>
		<div>
		  <input type="checkbox" id="check_t7" name="check_t7" onclick="clicbox(this.id)" {$liste_courbes['check_t7']}>
		  <label for="check_t7">T7 Coll Froid</label>
		</div>
		<div>
		  <input type="checkbox" id="check_t8" name="check_t8" onclick="clicbox(this.id)" {$liste_courbes['check_t8']}>
		  <label for="check_t8">T8 Coll Chaud</label>
		</div>
		</td>

		<td valign="top" style="padding-right:10px">
		<div>
		  <input type="checkbox" id="check_t9" name="check_t9" onclick="clicbox(this.id)" {$liste_courbes['check_t9']} >
		  <label for="check_t9">T9 Exterieure</label>
		</div>
		<div>
		  <input type="checkbox" id="check_t11" name="check_t11" onclick="clicbox(this.id)" {$liste_courbes['check_t11']} >
		  <label for="check_t11">T11 Maison</label>
		</div>
		<div>
		  <input type="checkbox" id="check_tcons1" name="check_tcons1" onclick="clicbox(this.id)" {$liste_courbes['check_tcons1']} >
		  <label for="check_tcons1">T Consigne</label>
		</div>
		</td>

		<td valign="top" style="padding-right:10px">
		<div>
		  <input type="checkbox" id="check_c4" name="check_c4" onclick="clicbox(this.id)" {$liste_courbes['check_c4']}>
		  <label for="check_c4">C4 BAL Appoint</label>
		</div>
		<div>
		  <input type="checkbox" id="check_c5" name="check_c5" onclick="clicbox(this.id)" {$liste_courbes['check_c5']}>
		  <label for="check_c5">C5 BAL Solaire</label>
		</div>
		<div>
		  <input type="checkbox" id="check_c1" name="check_c1" onclick="clicbox(this.id)" {$liste_courbes['check_c1']}>
		  <label for="check_c1">C1 ZONE 1</label>
		</div>
		<div>
		  <input type="checkbox" id="check_v3v" name="check_v3v" onclick="clicbox(this.id)" {$liste_courbes['check_v3v']}>
		  <label for="check_v3v">Vanne 3 voies</label>
		</div>
		<br />
		<div>
		  <input type="checkbox" id="grouper" name="grouper" onclick="clicbox_grouper()">
		  <label for="grouper">Regrouper les valeurs</label>
		</div>
		</td>
		
		<td valign="top" style="padding-right:10px">
		<div>
		  <input type="checkbox" id="check_chaudiere" name="check_chaudiere" onclick="clicbox(this.id)" {$liste_courbes['check_chaudiere']}
		  <label for="check_chaudiere">Chaudière</label>
		  <br /><br /><br />
			<input type="button" value="RAZ" onclick="verifie_series()">
		</div>
		</td>
	</tr>
	</table>
	</fieldset>

</td>

	
<td valign="top" style="padding-right:10px">
	<fieldset class="ui-widget-content">
	<legend class="ui-widget-header ui-state-default" style="border: none; background: none;">Données</legend>

		<table>
			<tr>
				<td>
				<input type="radio" id="duree" name="select_donnees" value="duree" {$select_duree}>
				Depuis <input type="text" size="1" id="nb_heures" name="nb_heures" value="{$nb_heures}" onClick="document.forms['form'].select_donnees[0].checked = true;"> heures
				</td>
			</tr>
			<tr>
				<td>
				<input type="radio" id="intervalle" name="select_donnees" value="intervale" {$select_intervale}>
				Du <input type="text" size="12" id="dateheure_debut" name="dateheure_debut" value="{$dateheure_debut_visu}" onClick="document.forms['form'].select_donnees[1].checked = true;"> au <input type="text" size="12" id="dateheure_fin" name="dateheure_fin" value="{$dateheure_fin_visu}" onClick="document.forms['form'].select_donnees[1].checked = true;">
				</td>
			</tr>
			<tr>
				<td valign="top" style="padding-right:10px">
				<input type="button" value="OK" onclick="updateChart(); UpdateTitle()">&nbsp&nbsp<input type="button" value="RAZ" onclick="init_saisie(); updateChart(); UpdateTitle()">

			</tr>
		</table>
	</fieldset>
	<br />&nbsp;Dernières données : <span id="derniere_donnee"></span><br />
	<span id='libelle_alerte' ></span> 
	&nbsp;<a href='/reel.php' target='_blank'>Temps réel</a><br>
	&nbsp;<a href='/' target='_blank'>Accueil</a><br>
</td>
</tr>
</table>
	</form>
	</div>
</div>

</body>


<script src="js/jquery.datetimepicker.full.min.js"></script>

{literal}
<script type="text/javascript" >
jQuery('#dateheure_debut').datetimepicker({
  format:'d/m/Y H:i',
  lang:'fr',
  step: 60
});

jQuery('#dateheure_fin').datetimepicker({
  format:'d/m/Y H:i',
  lang:'fr',
  step: 60
});

</script>
{/literal}

<script type="text/javascript" >
{include file='js/script.js'}
</script> 

<script type="text/javascript" >
function UpdateTitle() { graphique.setTitle({literal}{{/literal}text: "SolisArt - {$numero_installation}"{literal}}{/literal}); };
UpdateTitle();
</script>

</html>