# Solisart

Ce projet vous permet :
- de stocker les données de votre installation Solisart dans une base de données Mysql
- de restituer ces données dans un graphique accessible sur un site Web hébergée sur votre serveur
- d'afficher les données en temps réel sans passer par le site du constructeur, et de modifier les consignes facilement et sans mot de passe

# Installation

- Clônez le dépot sur votre serveur Web
- Créez une base Mysql et importez sa structure avec le fichier /sql/init_base_solisart.sql
- Importez la table paramètre pour les icones de météo : /sql/init_icones_weathermap.sql
- Modifiez le fichier de configuration /includes/config.inc.php.exemple pour y mettre vos données et renommez-le en config.inc.php
- Créez une première tâche cron pour exécuter régulièrement le script /capture_solisgraph.php (toutes les minutes par exemple) : récupère les données de l'installation
- Créez une deuxième tâche cron pour exécuter régulièrement le script /capture_weathermap.php (toutes les 15 minutes par exemple) : récupère les données de météo
- deux autres tâches peuvent être ajoutées pour la suppression des vieilles données et limiter la taille de la base : cleanup_solisgraph.php et cleanup_weathermap.php

# Utilisation

- la page par défaut index.php affiche les dernières données présentes en base ainsi que la météo
- la page solisgraph.php affiche le graphique
- la page reel.php affiche l'installation en temps réel (rafraîchie toutes les trois secondes) et permet de modifier certaines consignes

