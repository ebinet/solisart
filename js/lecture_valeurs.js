var heure_attente_1 = 0;
var heure_attente_2 = 0;
var heure_attente_3 = 0;
var heure_valeurs_installation = 0;
var tableau_valeurs_navigateur = [];
var tableau_valeurs_envoye = [];
var tableau_valeurs_serveur = [];
var tableau_heures_serveur = [];
var tableau_droit_modification_donnees = [];
var id_timer_lecture_valeurs = -1;
var mise_a_jour_en_cours = false;
var rafraichir = false;

// --------------------------------------
// RAFRAÎCHIR TOUTES LES DONNÉES
// --------------------------------------

function rafraichir_valeurs ()
{
	$("#contact-installation").html ("Date et heure de contact");
	$("#message-systeme").html ("");

	$("#check-chaudiere-1-label").html ("<img src=\"image/led_grise.png\" />&nbsp;&nbsp;En attente...");

	$("#consigne-chauffage option[value='0']").prop ('selected', true);
	$("#consigne-hp-chauffage").val ("00,0");
	$("#consigne-hc-chauffage").val ("00,0");
	
	$("#temoin_chauffage").html ("<img src=\"image/bullet_black.png\" />");
	$("#temoin_hp_chauffage").html ("<img src=\"image/bullet_black.png\" />");
	$("#temoin_hc_chauffage").html ("<img src=\"image/bullet_black.png\" />");
	$("#temoin_retour_chauffage").html ("<img src=\"image/bullet_green.png\" />");

	$("#consigne-ecs option[value='0']").prop ('selected', true);
	$("#consigne-hp-ecs").val ("00,0");
	$("#consigne-hc-ecs").val ("00,0");

	$("#temoin_ecs").html ("<img src=\"image/bullet_black.png\" />");
	$("#temoin_hp_ecs").html ("<img src=\"image/bullet_black.png\" />");
	$("#temoin_hc_ecs").html ("<img src=\"image/bullet_black.png\" />");
	$("#temoin_retour_ecs").html ("<img src=\"image/bullet_green.png\" />");

	$("#temp-valeur-1").html (": 00,0 °C");
	$("#temp-valeur-2").html (": 00,0 °C");
	$("#temp-valeur-3").html (": 00,0 °C");
	$("#temp-valeur-4").html (": 00,0 °C");
	$("#temp-valeur-7").html (": 00,0 °C");
	$("#temp-valeur-8").html (": 00,0 °C");
	$("#temp-valeur-9").html (": 00,0 °C");
	$("#temp-valeur-11").html (": 00,0 °C");

	$("#circ-valeur-1").html ("<img src=\"image/led_grise.png\" />");
	$("#circ-valeur-2").html ("<img src=\"image/led_grise.png\" />");
	$("#circ-valeur-5").html ("<img src=\"image/led_grise.png\" />");
	
	$("#v3v-valeur-1").html ("0 %");
	
	rafraichir = true;
}


// --------------------------------------
// MODIFIER LES CONSIGNES DE CHAUFFAGE
// --------------------------------------

function modifier_chauffage ()
{
	$("#temoin_retour_chauffage").html ("<img src=\"image/bullet_red.png\" />");
	var asynchrone = true;
	mise_a_jour_en_cours = true;
	var jqxhr = $.ajax ({ url: "modifier_consignes.php", type: "POST", data: {mode: "chauffage", consigne_chauffage: $("#consigne-chauffage").find('option:selected').val(), consigne_hp_chauffage: $("#consigne-hp-chauffage").val(), consigne_hc_chauffage: $("#consigne-hc-chauffage").val()}, success: afficher_resultat_succes, error: afficher_resultat_erreur, dataType: "xml", async: asynchrone});
	//	$("#contact-installation").html (jqxhr.done());
}

// --------------------------------------
// MODIFIER LES CONSIGNES ECS
// --------------------------------------

function modifier_ecs ()
{
	$("#temoin_retour_ecs").html ("<img src=\"image/bullet_red.png\" />");
	var asynchrone = true;
	mise_a_jour_en_cours = true;
	$.ajax ({ url: "modifier_consignes.php", type: "POST", data: {mode: "ecs", consigne_ecs: $("#consigne-ecs").find('option:selected').val(), consigne_hp_ecs: $("#consigne-hp-ecs").val(), consigne_hc_ecs: $("#consigne-hc-ecs").val()}, success: afficher_resultat_succes, error: afficher_resultat_erreur, dataType: "xml", async: asynchrone});
}

// -----------------------------------
// AFFICHAGE DU RESULTAT
// -----------------------------------

function afficher_resultat_succes (requete, reponse_statut, reponse_message)
{
	mise_a_jour_en_cours = false;
	$("#temoin_retour_ecs").html ("<img src=\"image/bullet_green.png\" />");
	$("#temoin_retour_chauffage").html ("<img src=\"image/bullet_green.png\" />");
	$("#message-systeme").html ("");
}

function afficher_resultat_erreur (requete, reponse_statut, reponse_message)
{
	mise_a_jour_en_cours = false;
	$("#message-systeme").html ("&nbsp-&nbspErreur : " + reponse_statut + " - " + reponse_message);
}

// --------------------------------------
// FONCTIONS LIÉES AUX MODIFICATIONS
// --------------------------------------

function modif_chauffage()
{
	$("#temoin_chauffage").html ("<img src=\"image/bullet_red.png\" />");
	$("#temoin_retour_chauffage").html ("<img src=\"image/bullet_yellow.png\" />");
}
function modif_hp_chauffage()
{
	$("#temoin_hp_chauffage").html ("<img src=\"image/bullet_red.png\" />");
	$("#temoin_retour_chauffage").html ("<img src=\"image/bullet_yellow.png\" />");

}
function modif_hc_chauffage()
{
	$("#temoin_hc_chauffage").html ("<img src=\"image/bullet_red.png\" />");
	$("#temoin_retour_chauffage").html ("<img src=\"image/bullet_yellow.png\" />");
}
function modif_ecs()
{
	$("#temoin_ecs").html ("<img src=\"image/bullet_red.png\" />");
	$("#temoin_retour_ecs").html ("<img src=\"image/bullet_yellow.png\" />");
}
function modif_hp_ecs()
{
	$("#temoin_hp_ecs").html ("<img src=\"image/bullet_red.png\" />");
	$("#temoin_retour_ecs").html ("<img src=\"image/bullet_yellow.png\" />");
}
function modif_hc_ecs()
{
	$("#temoin_hc_ecs").html ("<img src=\"image/bullet_red.png\" />");
	$("#temoin_retour_ecs").html ("<img src=\"image/bullet_yellow.png\" />");
}

// -----------------------------------
// LECTURE DES VALEURS
// -----------------------------------

function lire_valeurs_donnees ()
{
	var asynchrone = true;
	if (!mise_a_jour_en_cours)
	{
		$.ajax ({ url: "lecture_valeurs_donnees.php", type: "GET", data: {heure: heure_valeurs_installation}, success: lire_valeurs_donnees_callback_succes, error: lire_valeurs_donnees_callback_echec, dataType: "xml", async: asynchrone});
	}
}

// -----------------------------------
// RETOUR SUCCÈS
// -----------------------------------

function lire_valeurs_donnees_callback_succes (reponse_xml, reponse_statut, requete)
{
	var heure_courante = undefined;
	var heure_attente_tmp = undefined;

	if (reponse_statut == "success")
	{
		if ($(reponse_xml).find ("valeurs").attr ("statut") == "succes")
		{
			heure_courante = Math.floor (new Date ().getTime () / 1000);
			heure_contact = heure_courante - parseInt ($(reponse_xml).find ("valeurs").attr ("age_contact"));
			heure_attente_tmp = parseInt ($(reponse_xml).find ("valeurs").attr ("heure_contact"));
			if (heure_attente_tmp > heure_attente_1)
			{
				heure_attente_3 = heure_attente_2;
				heure_attente_2 = heure_attente_1;
				heure_attente_1 = heure_attente_tmp;
			}
			heure_valeurs_navigateur = heure_courante - parseInt ($(reponse_xml).find ("valeurs").attr ("age_valeurs"));
			if (rafraichir)
			{
				heure_valeurs_installation = 0;
				rafraichir = false;
			}
			else
				heure_valeurs_installation = parseInt ($(reponse_xml).find ("valeurs").attr ("heure_valeurs"));
			
			// Enregistrement des valeurs des données
			$(reponse_xml).find ("valeur").each (enregistrer_valeur_maj);
			periode_lecture_valeurs = 1;
			// Affichage des valeurs dans la page
			if (periode_lecture_valeurs > 0)
			{
				// Affichage des valeurs et des statuts
				$(reponse_xml).find ("valeur").each (afficher_valeur_et_statut);
				$("#contact-installation").html (formater_date (heure_contact) + " " + formater_heure (heure_contact, true));
			}

			// Affichage du statut de la communication avec l'installation
//			maj_statut_communication ();
		}
		else
		{
			enregistrer_message_communication ("Erreur lors de la lecture des valeurs des données", "Le serveur a retourné une erreur : '" + atob ($(reponse_xml).find ("valeurs").attr ("message")) + "'", 2, Math.floor (periode_lecture_valeurs / 1000));
		}
	}
	else
	{
		enregistrer_message_communication ("Erreur lors de la lecture des valeurs des données", "Une erreur (code HTTP " + requete.status + ") est survenue lors de la communication avec le serveur : " + requete.statusText, 2, Math.floor (periode_lecture_valeurs / 1000));
	}
	lecture_valeurs_en_cours = 0;
}

// -----------------------------------
// RETOUR ANOMALIE
// -----------------------------------

function lire_valeurs_donnees_callback_echec (requete, reponse_statut, reponse_message)
{
	enregistrer_message_communication ("Erreur lors de la lecture des valeurs des données", "Une erreur est survenue lors de la communication avec le serveur (" + reponse_statut + " - " + reponse_message + ")", 2, Math.floor (periode_lecture_valeurs / 1000));
	lecture_valeurs_en_cours = 0;
}

// Fonctions de gestion du statut de la communication

function enregistrer_message_communication (message, detail, statut, duree)
{

}


// -----------------------------------
// ENREGISTRER LES VALEURS
// -----------------------------------

function enregistrer_valeur_maj ()
{
	var heure = undefined;
	var donnee = undefined;
	var valeur = undefined;

	heure = parseInt ($(this).attr ("heure"));
	donnee = parseInt ($(this).attr ("donnee"));
	valeur = convertir_valeur (donnee, atob ($(this).attr ("valeur")));
	// Si les valeurs précédentes du navigateur et du serveur étaient identiques
	if (tableau_valeurs_navigateur [donnee] === tableau_valeurs_serveur [donnee])
	{
		// Enregistrement de la valeur dans le tableau "navigateur"
		tableau_valeurs_navigateur [donnee] = String (valeur);
	}
	// Suppression de la valeur dans le tableau "envoyé"
	tableau_valeurs_envoye [donnee] = undefined;
	// Enregistrement de la valeur dans le tableau "serveur"
	tableau_valeurs_serveur [donnee] = String (valeur);
	// Enregistrement de l'heure dans le tableau "serveur"
	tableau_heures_serveur [donnee] = heure;
}

function convertir_valeur (donnee, valeur)
{
	if ((donnee == donnee_NonRecupCal_0)
	||  (donnee == donnee_NonRecupCal_1)
	||  (donnee == donnee_Mode_Hors_Gel)
	||  ((donnee >= donnee_Mode_Derog_0) && (donnee <= donnee_Mode_Derog_6))
	||  (donnee == donnee_PriorAppoint1)
	||  (donnee == donnee_CacS48)
	||  (donnee == donnee_CacS49)
	||  ((donnee >= donnee_Mode_Derog_7) && (donnee <= donnee_Mode_Derog_19)))
	{
		if (valeur == "checked")
		{
			valeur = 1;
		}
		else
		{
			valeur = 0;
		}
	}
	return valeur;
}

// -----------------------------------
// AFFICHER LES VALEURS
// -----------------------------------

function afficher_valeur_et_statut ()
{
	var donnee = undefined;
	
	donnee = parseInt ($(this).attr ("donnee"));
	// Affichage de la valeur
	afficher_valeur (donnee);
	// Affichage du statut de la valeur
//	afficher_statut_valeur (donnee);
}

function afficher_libelle_temp (numero, valeur)
{
	$("#temp-libelle-" + numero).text (valeur);
}

function afficher_valeur_temp (numero, donnee_libelle, valeur)
{
	var id = undefined;
	var afficher = undefined;

	id = "#temp-valeur-" + numero;
	if ((tableau_valeurs_navigateur [donnee_libelle] == "") && ((valeur == "Dsc") || (valeur == undefined)))
	{
		$(id).text ("");
	}
	else
	{
		if (numero >= 11)
		{
			afficher = true;
		}
		else
		{
			afficher = true;
		}
		if (afficher == true)
		{
			if (valeur != undefined)
			{
				$(id).text (": " + valeur.replace (".", ",").replace ("dC", "°C"));
			}
			else
			{
				$(id).text (": ");
			}
		}
		else
		{
			$(id).text (": ---");
		}
	}
}

function verifier_droit(a,b)
{
	return true;
}

function afficher_debit (id, valeur)
{
	$(id).text (": " + valeur.replace (".", ","));
}

function afficher_circulateur (numero, valeur)
{
	var id = undefined;

	id = "#circ-valeur-" + numero;
	if (valeur == "0pC")
	{
		$(id).html ("<img src=\"image/led_rouge.png\" />");
	}
	else
	{
		if (valeur == "100pC")
		{
			$(id).html ("<img src=\"image/led_verte.png\" />");
		}
		else
		{
			if (valeur != undefined)
			{
				$(id).text (valeur.replace ("pC", " %"));
			}
			else
			{
				$(id).text ("? %");
			}
		}
	}
}

// Formatage d'une date
function formater_date (heure)
{
	var chaine = undefined;
	var date = undefined;
	var valeur = undefined;

	chaine = "";
	date = new Date (heure * 1000);
	if (date.getDate () < 10)
	{
		chaine += "0";
	}
	chaine += date.getDate () + "/";
	valeur = date.getMonth () + 1;
	if (valeur < 10)
	{
		chaine += "0";
	}
	chaine += valeur + "/" + date.getFullYear ();
	return chaine;
}

// Formatage d'une heure, avec ou sans les secondes
function formater_heure (heure, formater_secondes)
{
	var chaine = undefined;
	var date = undefined;

	chaine = "";
	date = new Date (heure * 1000);
	if (date.getHours () < 10)
	{
		chaine += "0";
	}
	chaine += date.getHours () + ":";
	valeur = date.getMinutes ();
	if (date.getMinutes () < 10)
	{
		chaine += "0";
	}
	chaine += date.getMinutes ();
	if (formater_secondes == true)
	{
		chaine += ":";
		if (date.getSeconds () < 10)
		{
			chaine += "0";
		}
		chaine += date.getSeconds ();
	}
	return chaine;
}

function afficher_valeur (donnee)
{
	var valeur = undefined;
	var entier = undefined;
	var numero = undefined;
	var texte = undefined;
	var max = undefined;

	valeur = tableau_valeurs_navigateur [donnee];
	switch (donnee)
	{
		case donnee_tlbl_11 : // tlbl(11)
			$("#input-libelles-tlbl11").val (valeur);
			afficher_libelle_temp (11, valeur);
			afficher_valeur_temp (11, donnee_tlbl_11, tableau_valeurs_navigateur [donnee_t_11]);
			break;
		case donnee_tlbl_12 : // tlbl(12)
			$("#input-libelles-tlbl12").val (valeur);
			afficher_libelle_temp (12, valeur);
			afficher_valeur_temp (12, donnee_tlbl_12, tableau_valeurs_navigateur [donnee_t_12]);
			break;
		case donnee_tlbl_13 : // tlbl(13)
			$("#input-libelles-tlbl13").val (valeur);
			afficher_libelle_temp (13, valeur);
			afficher_valeur_temp (13, donnee_tlbl_13, tableau_valeurs_navigateur [donnee_t_13]);
			break;
		case donnee_tlbl_14 : // tlbl(14)
			$("#input-libelles-tlbl14").val (valeur);
			afficher_libelle_temp (14, valeur);
			afficher_valeur_temp (14, donnee_tlbl_14, tableau_valeurs_navigateur [donnee_t_14]);
			break;
		case donnee_tlbl_01 : // tlbl(01)
			$("#input-libelles-tlbl1").val (valeur);
			afficher_libelle_temp (1, valeur);
			afficher_valeur_temp (1, donnee_tlbl_01, tableau_valeurs_navigateur [donnee_t_1]);
			break;
		case donnee_tlbl_09 : // tlbl(09)
			$("#input-libelles-tlbl9").val (valeur);
			afficher_libelle_temp (9, valeur);
			afficher_valeur_temp (9, donnee_tlbl_09, tableau_valeurs_navigateur [donnee_t_9]);
			break;
		case donnee_tlbl_02 : // tlbl(02)
			$("#input-libelles-tlbl2").val (valeur);
			afficher_libelle_temp (2, valeur);
			afficher_valeur_temp (2, donnee_tlbl_02, tableau_valeurs_navigateur [donnee_t_2]);
			break;
		case donnee_tlbl_10 : // tlbl(10)
			$("#input-libelles-tlbl10").val (valeur);
			afficher_libelle_temp (10, valeur);
			afficher_valeur_temp (10, donnee_tlbl_10, tableau_valeurs_navigateur [donnee_t_10]);
			break;
		case donnee_tlbl_03 : // tlbl(03)
			$("#input-libelles-tlbl3").val (valeur);
			afficher_libelle_temp (3, valeur);
			afficher_valeur_temp (3, donnee_tlbl_03, tableau_valeurs_navigateur [donnee_t_3]);
			break;
		case donnee_tlbl_04 : // tlbl(04)
			$("#input-libelles-tlbl4").val (valeur);
			afficher_libelle_temp (4, valeur);
			afficher_valeur_temp (4, donnee_tlbl_04, tableau_valeurs_navigateur [donnee_t_4]);
			break;
		case donnee_tlbl_05 : // tlbl(05)
			$("#input-libelles-tlbl5").val (valeur);
			afficher_libelle_temp (5, valeur);
			afficher_valeur_temp (5, donnee_tlbl_05, tableau_valeurs_navigateur [donnee_t_5]);
			break;
		case donnee_tlbl_06 : // tlbl(06)
			$("#input-libelles-tlbl6").val (valeur);
			afficher_libelle_temp (6, valeur);
			afficher_valeur_temp (6, donnee_tlbl_06, tableau_valeurs_navigateur [donnee_t_6]);
			break;
		case donnee_tlbl_07 : // tlbl(07)
			$("#input-libelles-tlbl7").val (valeur);
			afficher_libelle_temp (7, valeur);
			afficher_valeur_temp (7, donnee_tlbl_07, tableau_valeurs_navigateur [donnee_t_7]);
			break;
		case donnee_tlbl_15 : // tlbl(15)
			$("#input-libelles-tlbl15").val (valeur);
			afficher_libelle_temp (15, valeur);
			afficher_valeur_temp (15, donnee_tlbl_15, tableau_valeurs_navigateur [donnee_t_15]);
			break;
		case donnee_tlbl_08 : // tlbl(08)
			$("#input-libelles-tlbl8").val (valeur);
			afficher_libelle_temp (8, valeur);
			afficher_valeur_temp (8, donnee_tlbl_08, tableau_valeurs_navigateur [donnee_t_8]);
			break;
		case donnee_tlbl_16 : // tlbl(16)
			$("#input-libelles-tlbl16").val (valeur);
			afficher_libelle_temp (16, valeur);
			afficher_valeur_temp (16, donnee_tlbl_16, tableau_valeurs_navigateur [donnee_t_16]);
			break;
		case donnee_fllbl_1 : // fllbl(1)
			$("#input-libelles-fllbl1").val (valeur);
			$("#debit-libelle-1").text (valeur);
			break;
		case donnee_fllbl_2 : // fllbl(2)
			$("#input-libelles-fllbl2").val (valeur);
			$("#debit-libelle-2").text (valeur);
			break;
		case donnee_fllbl_3 : // fllbl(3)
			$("#input-libelles-fllbl3").val (valeur);
			$("#debit-libelle-3").text (valeur);
			break;
		case donnee_fllbl_4 : // fllbl(4)
			$("#input-libelles-fllbl4").val (valeur);
			$("#debit-libelle-4").text (valeur);
			break;
		case donnee_fllbl_5 : // fllbl(5)
			$("#input-libelles-fllbl5").val (valeur);
			$("#debit-libelle-5").text (valeur);
			break;
		case donnee_fllbl_6 : // fllbl(6)
			$("#input-libelles-fllbl6").val (valeur);
			$("#debit-libelle-6").text (valeur);
			break;
		case donnee_trilbl_01 : // trilbl(01)
			$("#input-libelles-trilbl1").val (valeur);
			$("#circ-libelle-1").text (valeur);
			break;
		case donnee_trilbl_08 : // trilbl(08)
			$("#input-libelles-trilbl8").val (valeur);
			$("#circ-libelle-8").text (valeur);
			break;
		case donnee_trilbl_02 : // trilbl(02)
			$("#input-libelles-trilbl2").val (valeur);
			$("#circ-libelle-2").text (valeur);
			break;
		case donnee_trilbl_09 : // trilbl(09)
			$("#input-libelles-trilbl9").val (valeur);
			$("#circ-libelle-9").text (valeur);
			break;
		case donnee_trilbl_03 : // trilbl(03)
			$("#input-libelles-trilbl3").val (valeur);
			$("#circ-libelle-3").text (valeur);
			break;
		case donnee_trilbl_10 : // trilbl(10)
			$("#input-libelles-trilbl10").val (valeur);
			$("#circ-libelle-10").text (valeur);
			break;
		case donnee_trilbl_04 : // trilbl(04)
			$("#input-libelles-trilbl4").val (valeur);
			$("#circ-libelle-4").text (valeur);
			break;
		case donnee_trilbl_11 : // trilbl(11)
			$("#input-libelles-trilbl11").val (valeur);
			$("#circ-libelle-11").text (valeur);
			break;
		case donnee_trilbl_05 : // trilbl(05)
			$("#input-libelles-trilbl5").val (valeur);
			$("#circ-libelle-5").text (valeur);
			break;
		case donnee_trilbl_12 : // trilbl(12)
			$("#input-libelles-trilbl12").val (valeur);
			$("#circ-libelle-12").text (valeur);
			break;
		case donnee_trilbl_06 : // trilbl(06)
			$("#input-libelles-trilbl6").val (valeur);
			$("#circ-libelle-6").text (valeur);
			break;
		case donnee_trilbl_13 : // trilbl(13)
			$("#input-libelles-trilbl13").val (valeur);
			$("#circ-libelle-13").text (valeur);
			break;
		case donnee_trilbl_07 : // trilbl(07)
			$("#input-libelles-trilbl7").val (valeur);
			$("#circ-libelle-7").text (valeur);
			break;
		case donnee_ModeleDebitmetre_0 : // ModeleDebitmetre(0)
		case donnee_ModeleDebitmetre_1 : // ModeleDebitmetre(1)
		case donnee_ModeleDebitmetre_2 : // ModeleDebitmetre(2)
		case donnee_ModeleDebitmetre_3 : // ModeleDebitmetre(3)
		case donnee_ModeleDebitmetre_4 : // ModeleDebitmetre(4)
			texte = "?";
			switch (donnee)
			{
				case donnee_ModeleDebitmetre_0 : // ModeleDebitmetre(0)
					numero = 1;
					break;
				case donnee_ModeleDebitmetre_1 : // ModeleDebitmetre(1)
					numero = 2;
					break;
				case donnee_ModeleDebitmetre_2 : // donnee_ModeleDebitmetre(2)
					numero = 3;
					break;
				case donnee_ModeleDebitmetre_3 : // ModeleDebitmetre(3)
					numero = 4;
					break;
				case donnee_ModeleDebitmetre_4 : // ModeleDebitmetre(4)
					numero = 5;
					break;
			}
			entier = parseInt (valeur);
			switch (entier)
			{
				case 0 :
					texte = "Vortex DN20";
					break;
				case 1 :
					texte = "Vortex DN25";
					break;
				case 2 :
					texte = "Vortex DN30";
					break;
				case 3 :
					texte = "1l/impulsion";
					break;
				case 4 :
					texte = "2,5l/impulsion";
					break;
				case 5 :
					texte = "5l/impulsion";
					break;
				case 6 :
					texte = "10l/impulsion";
					break;
				case 7 :
					texte = "25l/impulsion";
					break;
				case 8 :
					texte = "50l/impulsion";
					break;
				case 9 :
					texte = "100l/impulsion";
					break;
			}
			break;
		case donnee_CircType_0 : // CircType(0)
		case donnee_CircType_1 : // CircType(1)
		case donnee_CircType_2 : // CircType(2)
		case donnee_CircType_3 : // CircType(3)
		case donnee_CircType_4 : // CircType(4)
		case donnee_CircType_5 : // CircType(5)
		case donnee_CircType_6 : // CircType(6)
		case donnee_CircType_9 : // CircType(9)
		case donnee_CircType_10 : // CircType(10)
		case donnee_CircType_13 : // CircType(13)
		case donnee_CircType_14 : // CircType(14)
		case donnee_CircType_15 : // CircType(15)
		case donnee_CircType_16 : // CircType(16)
		case donnee_CircType_17 : // CircType(17)
		case donnee_CircType_18 : // CircType(18)
		case donnee_CircType_19 : // CircType(19)
		case donnee_CircType_20 : // CircType(20)
		case donnee_CircType_21 : // CircType(21)
		case donnee_CircType_22 : // CircType(22)
		case donnee_CircType_23 : // CircType(23)
		case donnee_CircType_24 : // CircType(24)
		case donnee_CircType_25 : // CircType(25)
			texte = "?";
			if (donnee <= donnee_CircType_6)
			{
				numero = donnee - donnee_CircType_0 + 1;
			}
			else
			{
				if (donnee <= donnee_CircType_10)
				{
					numero = donnee - donnee_CircType_9 + 10;
				}
				else
				{
					numero = donnee - donnee_CircType_13 + 13;
				}
			}
			if ((donnee <= donnee_CircType_10) || (tableau_valeurs_navigateur [donnee_Mode] == 1))
			{
				entier = parseInt (valeur);
				switch (entier)
				{
					case 1 :
						texte = "BC V1";
						break;
					case 2 :
						texte = "BC V2";
						break;
					case 3 :
						texte = "BC V3";
						break;
					case 4 :
						texte = "BC V4";
						break;
					case 5 :
						texte = "BC V5";
						break;
					case 10 :
						texte = "V2V Z1";
						break;
					case 11 :
						texte = "V2V Z2";
						break;
					case 12 :
						texte = "V2V Z3";
						break;
					case 13 :
						texte = "V2V Z4";
						break;
					default :
						if (entier != 0)
						{
							entier = 0;
							enregistrer_valeur (donnee, entier);
						}
						texte = "TRAD";
				}
				$("#input-circulateurs-" + numero).text (texte);


					max = 5;
					if (numero >= 13)
					{
						max = 13;
						if (tableau_valeurs_navigateur [donnee_Type_Emetteur_3] != 10)
						{
							max = 12;
							if (tableau_valeurs_navigateur [donnee_Type_Emetteur_2] != 10)
							{
								max = 11;
								if (tableau_valeurs_navigateur [donnee_Type_Emetteur_1] != 10)
								{
									max = 10;
									if (tableau_valeurs_navigateur [donnee_Type_Emetteur_0] != 10)
									{
										max = 5;
									}
								}
							}
						}
					}

			}
			break;
		case donnee_t_1 : // t(1)
			afficher_libelle_temp (1, tableau_valeurs_navigateur [donnee_tlbl_01]);
			afficher_valeur_temp (1, donnee_tlbl_01, valeur);
			break;
		case donnee_t_2 : // t(2)
			afficher_libelle_temp (2, tableau_valeurs_navigateur [donnee_tlbl_02]);
			afficher_valeur_temp (2, donnee_tlbl_02, valeur);
			break;
		case donnee_t_3 : // t(3)
			afficher_libelle_temp (3, tableau_valeurs_navigateur [donnee_tlbl_03]);
			afficher_valeur_temp (3, donnee_tlbl_03, valeur);
			break;
		case donnee_t_4 : // t(4)
			afficher_libelle_temp (4, tableau_valeurs_navigateur [donnee_tlbl_04]);
			afficher_valeur_temp (4, donnee_tlbl_04, valeur);
			break;
		case donnee_t_5 : // t(5)
			afficher_libelle_temp (5, tableau_valeurs_navigateur [donnee_tlbl_05]);
			afficher_valeur_temp (5, donnee_tlbl_05, valeur);
			break;
		case donnee_t_6 : // t(6)
			afficher_libelle_temp (6, tableau_valeurs_navigateur [donnee_tlbl_06]);
			afficher_valeur_temp (6, donnee_tlbl_06, valeur);
			break;
		case donnee_t_7 : // t(7)
			afficher_libelle_temp (7, tableau_valeurs_navigateur [donnee_tlbl_07]);
			afficher_valeur_temp (7, donnee_tlbl_07, valeur);
			break;
		case donnee_t_8 : // t(8)
			afficher_libelle_temp (8, tableau_valeurs_navigateur [donnee_tlbl_08]);
			afficher_valeur_temp (8, donnee_tlbl_08, valeur);
			break;
		case donnee_t_9 : // t(9)
			afficher_libelle_temp (9, tableau_valeurs_navigateur [donnee_tlbl_09]);
			afficher_valeur_temp (9, donnee_tlbl_09, valeur);
			break;
		case donnee_t_10 : // t(10)
			afficher_libelle_temp (10, tableau_valeurs_navigateur [donnee_tlbl_10]);
			afficher_valeur_temp (10, donnee_tlbl_10, valeur);
			break;
		case donnee_t_11 : // t(11)
			afficher_libelle_temp (11, tableau_valeurs_navigateur [donnee_tlbl_11]);
			afficher_valeur_temp (11, donnee_tlbl_11, valeur);
			break;
		case donnee_t_12 : // t(12)
			afficher_libelle_temp (12, tableau_valeurs_navigateur [donnee_tlbl_12]);
			afficher_valeur_temp (12, donnee_tlbl_12, valeur);
			break;
		case donnee_t_13 : // t(13)
			afficher_libelle_temp (13, tableau_valeurs_navigateur [donnee_tlbl_13]);
			afficher_valeur_temp (13, donnee_tlbl_13, valeur);
			break;
		case donnee_t_14 : // t(14)
			afficher_libelle_temp (14, tableau_valeurs_navigateur [donnee_tlbl_14]);
			afficher_valeur_temp (14, donnee_tlbl_14, valeur);
			break;
		case donnee_t_15 : // t(15)
			afficher_libelle_temp (15, tableau_valeurs_navigateur [donnee_tlbl_15]);
			afficher_valeur_temp (15, donnee_tlbl_15, valeur);
			break;
		case donnee_t_16 : // t(16)
			afficher_libelle_temp (16, tableau_valeurs_navigateur [donnee_tlbl_16]);
			afficher_valeur_temp (16, donnee_tlbl_16, valeur);
			break;
		case donnee_fl_1 : // fl(1)
			$("#debit-valeur-1").text (": " + valeur.replace (".", ","));
			break;
		case donnee_fl_2 : // fl(2)
			$("#debit-valeur-2").text (": " + valeur.replace (".", ","));
			break;
		case donnee_fl_3 : // fl(3)
			$("#debit-valeur-3").text (": " + valeur.replace (".", ","));
			break;
		case donnee_fl_4 : // fl(4)
			$("#debit-valeur-4").text (": " + valeur.replace (".", ","));
			break;
		case donnee_fl_5 : // fl(5)
			$("#debit-valeur-5").text (": " + valeur.replace (".", ","));
			break;
		case donnee_fl_6 : // fl(6)
			$("#debit-valeur-6").text (": " + valeur.replace (".", ","));
			break;
		case donnee_rl_0 : // rl(0)
			if (valeur == "On")
			{
				$("#check-chaudiere-1-label").html ("<img src=\"image/led_verte.png\" />&nbsp;&nbsp;<span style='color:green'><b>Marche</b></span>");
//				$("#check-chaudiere-1").prop ("checked", true).button ("refresh");
			}
			else
			{
				$("#check-chaudiere-1-label").html ("<img src=\"image/led_rouge.png\" />&nbsp;&nbsp;Arrêt");
//				$("#check-chaudiere-1").prop ("checked", false).button ("refresh");
			}
			break;
		case donnee_tr_1 : // tr(1)
		case donnee_tr_2 : // tr(2)
		case donnee_tr_3 : // tr(3)
		case donnee_tr_4 : // tr(4)
		case donnee_tr_5 : // tr(5)
		case donnee_tr_6 : // tr(6)
		case donnee_tr_7 : // tr(7)
		case donnee_tr_8 : // tr(8)
		case donnee_tr_9 : // tr(9)
		case donnee_tr_10 : // tr(10)
		case donnee_tr_11 : // tr(11)
		case donnee_tr_12 : // tr(12)
		case donnee_tr_13 : // tr(13)
			numero = donnee - donnee_tr_1 + 1;
			afficher_circulateur (numero, valeur);
			break;
		case donnee_v3v_0 : // v3v(0)
			$("#v3v-valeur-0").text (valeur + " %");
			break;
		case donnee_v3v_1 : // v3v(1)
			$("#v3v-valeur-1").text (valeur + " %");
			break;
		case donnee_flpws : // flpws
			switch (parseInt (valeur))
			{
				case 1 : // LED verte
					$("#debits-led").attr ("src", "image/led_verte.png");
					break;
				case 2 : // LED rouge
					$("#debits-led").attr ("src", "image/led_rouge.png");
					break;
				default : // LED grise
					$("#debits-led").attr ("src", "image/led_grise.png");
			}
			break;
		case donnee_tlbl_17 :
		case donnee_tlbl_18 :
		case donnee_tlbl_19 :
		case donnee_tlbl_20 :
		case donnee_tlbl_21 :
		case donnee_tlbl_22 :
		case donnee_tlbl_23 :
		case donnee_tlbl_24 :
		case donnee_tlbl_25 :
		case donnee_tlbl_26 :
		case donnee_tlbl_27 :
		case donnee_tlbl_28 :
		case donnee_tlbl_29 :
			if (tableau_valeurs_navigateur [donnee_Mode] == 1)
			{
				numero = donnee - donnee_tlbl_17 + 17;
				$("#input-libelles-tlbl" + numero).val (valeur);
				afficher_libelle_temp (numero, valeur);
				afficher_valeur_temp (numero, donnee, tableau_valeurs_navigateur [donnee_t_17 + numero - 17]);
			}
			break;
		case donnee_Mode : // mode
			if (parseInt (valeur) == 1)
			{
				$(".carte_fille").show ();
			}
			else
			{
				$(".carte_fille").hide ();
			}
			break
		case donnee_Type_Emetteur_0 : // Type_Emetteur(0)
		case donnee_Type_Emetteur_1 : // Type_Emetteur(1)
		case donnee_Type_Emetteur_2 : // Type_Emetteur(2)
		case donnee_Type_Emetteur_3 : // Type_Emetteur(3)
			if ((valeur != 10) && (tableau_valeurs_navigateur [donnee_Mode] == 1))
			{
				valeur = donnee - donnee_Type_Emetteur_0 + 10; // V2V Z1/2/3/4
				donnee = donnee_CircType_13;
				while (donnee <= donnee_CircType_25)
				{
					if (tableau_valeurs_navigateur [donnee] == valeur)
					{
						enregistrer_valeur (donnee, 0);
					}
					donnee++;
				}
			}
			break;
		case donnee_t_17 : // t(17)
		case donnee_t_18 : // t(18)
		case donnee_t_19 : // t(19)
		case donnee_t_20 : // t(20)
		case donnee_t_21 : // t(21)
		case donnee_t_22 : // t(22)
		case donnee_t_23 : // t(23)
		case donnee_t_24 : // t(24)
		case donnee_t_25 : // t(25)
		case donnee_t_26 : // t(26)
		case donnee_t_27 : // t(27)
		case donnee_t_28 : // t(28)
		case donnee_t_29 : // t(29)
			if (tableau_valeurs_navigateur [donnee_Mode] == 1)
			{
				numero = donnee - donnee_t_17 + 17;
				afficher_libelle_temp (numero, tableau_valeurs_navigateur [donnee_tlbl_17 + numero - 17]);
				afficher_valeur_temp (numero, donnee_tlbl_17 + numero - 17, valeur);
			}
			break;
		case donnee_tr_14 : // tr(14)
		case donnee_tr_15 : // tr(15)
		case donnee_tr_16 : // tr(16)
		case donnee_tr_17 : // tr(17)
		case donnee_tr_18 : // tr(18)
		case donnee_tr_19 : // tr(19)
		case donnee_tr_20 : // tr(20)
		case donnee_tr_21 : // tr(21)
		case donnee_tr_22 : // tr(22)
		case donnee_tr_23 : // tr(23)
		case donnee_tr_24 : // tr(24)
		case donnee_tr_25 : // tr(25)
		case donnee_tr_26 : // tr(26)
			afficher_circulateur (donnee - donnee_tr_14 + 14, valeur);
			break;

		case donnee_TConfort_0 : // TConfort(0)
			if (valeur == "")
			{
				$("#consigne-hp-chauffage").val ("-");
			}
			else
			{
				$("#consigne-hp-chauffage").val (String (valeur).replace (".", ",") );
				$("#temoin_hp_chauffage").html ("<img src=\"image/bullet_green.png\" />");

			}
			break;
		case donnee_TReduit_0 :
			if (valeur == "")
			{
				$("#consigne-hc-chauffage").val ("-");
			}
			else
			{
				$("#consigne-hc-chauffage").val (String (valeur).replace (".", ",") );
				$("#temoin_hc_chauffage").html ("<img src=\"image/bullet_green.png\" />");
			}
			break;
		case donnee_TConfort_6 :
			if (valeur == "")
			{
				$("#consigne-hp-ecs").val ("-");
			}
			else
			{
				$("#consigne-hp-ecs").val (String (valeur).replace (".", ",") );
				$("#temoin_hp_ecs").html ("<img src=\"image/bullet_green.png\" />");
			}
			break;
		case donnee_TReduit_6 :
			if (valeur == "")
			{
				$("#consigne-hc-ecs").val ("-");
			}
			else
			{
				$("#consigne-hc-ecs").val (String (valeur).replace (".", ",") );
				$("#temoin_hc_ecs").html ("<img src=\"image/bullet_green.png\" />");
			}
			break;
		case donnee_Mode_Chauff_0 :
			$("#consigne-chauffage option[value='"+valeur+"']").prop ('selected', true);
			$("#temoin_chauffage").html ("<img src=\"image/bullet_green.png\" />");
			break;
		
		case donnee_Mode_Chauff_6 :
			$("#consigne-ecs option[value='"+valeur+"']").prop ('selected', true);
			$("#temoin_ecs").html ("<img src=\"image/bullet_green.png\" />");
			break;

	}
}



// -----------------------------------
// ----------- DÉBUT -----------------
// -----------------------------------

function document_ready ()
{
	// Lecture des valeurs initiales des données
	lire_valeurs_donnees ();
	// Mise à jour du statut de la communication
//	maj_statut_communication ();
	// Programmation de la mise à jour du statut de la communication
//	id_timer_maj_statut_communication = setInterval (timer_maj_statut_communication, periode_maj_statut_communication);
	id_timer_lecture_valeurs = setInterval (lire_valeurs_donnees, 3000);
}

$(document).ready (document_ready);
