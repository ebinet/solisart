// -----------------------------------------------
// FABRICATION DE L'URL POUR TROUVER LES DONNÉES
// -----------------------------------------------

function fabriquer_url_donnees() {
	var url_courte = "{$url_data_sans_parametre}";
	if ($('input[name=select_donnees]:checked').val() == 'duree')
		return url_courte + '?type=duree&nb_heures=' + $("#nb_heures").val();
	else if ($('input[name=select_donnees]:checked').val() == 'intervale')
		return url_courte + '?type=intervale&debut=' + convertit_date_amj_heure_timestamp($("#date_debut").val(),$("#heure_debut").val()) + '&fin=' +  convertit_date_amj_heure_timestamp($("#date_fin").val(),$("#heure_fin").val());
	else 
		alert("La valeur du bouton radio est inconnue");
}

// Fonction de conversion de la saisie du paramètre date-heure (au format jj/mm/aaa hh:mm) en timestamp numérique
function convertit_date_heure_timestamp(date_heure) {
	tableau_date_heure = date_heure.split (" ");
	tableau_date = tableau_date_heure [0].split ("/");
	tableau_heure = tableau_date_heure [1].split (":");

	return Math.floor (new Date (tableau_date [2], parseInt (tableau_date [1]) - 1, tableau_date [0], tableau_heure [0], tableau_heure [1], 0, 0).valueOf () / 1000);
}

// Fonction de conversion de la saisie du paramètre date (au format aaaa-mm-jj) et heure (hh:mm) en timestamp numérique
function convertit_date_amj_heure_timestamp(date,heure) {
	tableau_date = date.split ("-");
	tableau_heure = heure.split (":");
	return Math.floor (new Date (tableau_date [0], parseInt (tableau_date [1]) - 1, tableau_date [2], tableau_heure [0], tableau_heure [1], 0, 0).valueOf () / 1000);
}

// -----------------------------------------
// CRÉATION ET MISE À JOUR DU GRAPHIQUE
// -----------------------------------------

function updateChart() {
//	console.log(fabriquer_url_donnees());
	graphique = Highcharts.chart('container', {
	
		data: {
			// L'URL est désormais paramétrée par le javascript
	        csvURL: fabriquer_url_donnees(),
	        beforeParse: function (csv) {
	            return csv.replace(/\n\n/g, '\n');
	        }
	    },
	    chart: {
	    	backgroundColor: '#c0c0c0',
	        zoomType: 'x'
		},
	
	    title: {
	        text: 'Solisart'
	    },
	
	    xAxis: {
	        type: 'datetime'
	    },
	
	    yAxis: {
	        title: {
	            text: null
	        }
	    },
	    
	    plotOptions: {
	        series: {
	            states: {
	            	inactive: {
				        opacity: 1
	     			},
	            }
	        }
	    },
	    
	    tooltip: {
	        valueSuffix: '°C',
	        crosshairs: true,
	        xDateFormat: '%A %e %b %H:%M',
	    },
	
		legend: {
			enabled:false
		},
	
	    series: [{
	    	name: 'T1 Capt Chaud 1',
	    	id: 't1',
	    	color: '#FF0000',
	        lineWidth: 1.5,
	      visible: $(reference_checkbox[0].nomjq).is(':checked')
	    },
		{
	    	name: 'T2 Capt Froid',
	    	id: 't2',
			color: '#00ff00',
			lineWidth: 1.5,
			visible: $(reference_checkbox[1].nomjq).is(':checked')
		},
		{
	    	name: 'T3 Bal Solaire',
	    	id: 't3',
			color: '#FFFF00',
			lineWidth: 1.5,
	      visible: $(reference_checkbox[2].nomjq).is(':checked')
		},
		{
	    	name: 'T4 Bal Appoint',
	    	id: 't4',
			color: '#0000FF',
			lineWidth: 1.5,
	      visible: $(reference_checkbox[3].nomjq).is(':checked')
		},
		{
	    	name: 'T7 Coll Froid',
	    	id: 't7',
			color: '#4c2085',
			lineWidth: 1.5,
			visible: $(reference_checkbox[4].nomjq).is(':checked')
		},
		{
	    	name: 'T8 Coll Chaud',
	    	id: 't8',
			color: '#863c3c',
			lineWidth: 1.5,
			visible: $(reference_checkbox[5].nomjq).is(':checked')
		},
		{
	    	name: 'T9 Exterieure',
	    	id: 't9',
			color: '#17147b',
			lineWidth: 1.5,
			visible: $(reference_checkbox[6].nomjq).is(':checked')
		},
		{
	    	name: 'T11 Maison',
	    	id: 't11',
			color: '#878711',
			lineWidth: 1.5,
			visible: $(reference_checkbox[7].nomjq).is(':checked')
		},            
		{
	    	name: 'T Consigne',
	    	id: 't11',
			color: '#878780',
			lineWidth: 1.5,
			visible: $(reference_checkbox[8].nomjq).is(':checked')
		},            
		{
	    	name: 'Vanne 3 voies',
	    	id: 'v3v',
			color: '#d9ea4d',
			lineWidth: 1.5,
			visible: $(reference_checkbox[9].nomjq).is(':checked'),
			tooltip: {
	        	valueSuffix: '%',
	   	        dateTimeLabelFormats: {
					hour: '%A %e %b %H:%M'
	    	    } 
	    	}
		},
		{
	    	name: 'C1 ZONE 1',
	    	id: 'c1',
			color: '#64c832',
			lineWidth: 1.5,
			visible: $(reference_checkbox[10].nomjq).is(':checked'),
			tooltip: {
	        	valueSuffix: '%'
	    	}
		},
		{
	    	name: 'C4 BAL Appoint',
	    	id: 'c4',
			color: '#7bea38',
			lineWidth: 1.5,
			visible: $(reference_checkbox[11].nomjq).is(':checked'),
			tooltip: {
	        	valueSuffix: '%'
	    	}
		},
		{
	    	name: 'C5 BAL Solaire',
	    	id: 'c5',
			color: '#ebeb7c',
			lineWidth: 1.5,
			visible: $(reference_checkbox[12].nomjq).is(':checked'),
			tooltip: {
	        	valueSuffix: '%'
	    	}
		},
		{
	    	name: 'Chaudière',
	    	id: 'chaudiere',
			color: '#000000',
			lineWidth: 1.5,
			visible: $(reference_checkbox[13].nomjq).is(':checked'),
			tooltip: {
	        	valueSuffix: '%'
	    	}
		}
		            
	    ]
	
	});
	
	// On affiche ou non les séries en fonction des cases cochées
//	verifie_series();
	
	// Appel de la date de la dernière donnée envoyée
	$.ajax ({
		// L'URL est paramétrée dans le script PHP
		url: "{$url_derniere_donnee}",
		type: "GET",
		dataType : 'html',
		success: function(data) {
			// Mise à jour de la variable globale
			derniere_donnee = data;

			// Affichage de la dernière donnée reçue
			$("#derniere_donnee").html (formater_date_heure (data));

			// Affiche le messsage d'erreur si les données ne sont pas à jour
			verifier_intervalle_derniere_donnee(data);

			// Réinitialise l'intervalle de saisie avec la dernière donnée, si ce n'est pas le critère en cours
			init_saisie_intervalle();
			}
		});

}

// -----------------------------------------
// OPTIONS DE LANGUES ET FUSEAU HORAIRE
// -----------------------------------------

Highcharts.setOptions({
    lang: {
        months: [
            'Janvier', 'Février', 'Mars', 'Avril',
            'Mai', 'Juin', 'Juillet', 'Août',
            'Septembre', 'Octobre', 'Novembre', 'Décembre'
        ],
        shortMonths: ["Jan", "Fev", "Mar", "Avr", "Mai", "Juin", "Juil", "Août", "Sept", "Oct", "Nov", "Dec"],
        weekdays: [
            'Dimanche', 'Lundi', 'Mardi', 'Mercredi',
            'Jeudi', 'Vendredi', 'Samedi'
        ],
        decimalPoint: ','
    },
    time: {
        /**
         * Use moment-timezone.js to return the timezone offset for individual
         * timestamps, used in the X axis labels and the tooltip header.
         */
        getTimezoneOffset: function (timestamp) {
            var zone = 'Europe/Paris',
                timezoneOffset = -moment.tz(timestamp, zone).utcOffset();

            return timezoneOffset;
        }
    }
});

// -----------------------------------------
// INITIALISATION DES CASES À COCHER
// -----------------------------------------

function	init_checkbox() {
	for ( const element of reference_checkbox)
		// si la case à cocher n'est pas conforme, on la met comme il faut
		if ($(element.nomjq).is(':checked') != element.valeur)
			$(element.nomjq).prop('checked', element.valeur);
}

// -----------------------------------------
// REMISE À NIVEAU DE L'AFFICHAGE DES SÉRIES
// -----------------------------------------

function verifie_series() {

	for ( const element of reference_checkbox) {
		// si la case à cocher n'est pas conforme, on la met comme il faut
		if ($(element.nomjq).is(':checked') != element.valeur)
			$(element.nomjq).prop('checked', element.valeur);

		// et on remet la série en cohérence si elle ne l'est pas non plus
		if (graphique.series[element.serie].visible != element.valeur)
			if (element.valeur)
				graphique.series[element.serie].show();
			else
				graphique.series[element.serie].hide();
	};
}

// -----------------------------------------
// FONCTIONS DE CLIC SUR LES CASES À COCHER
// -----------------------------------------

// CLIC SUR LES CASES D'AFFICHAGE DES SÉRIES

function clicbox(id_checkbox) {
	// Recherche de l'élément dans le tableau des cases à cocher
	checkbox_focus = reference_checkbox.find( element => element.id == id_checkbox);
	
	// Si la case à cocher n'est pas cohérente avec l'affichage de la série
	// on change l'affichage de la série
	if (graphique.series[checkbox_focus.serie].visible != $(checkbox_focus.nomjq).is(':checked'))
		if ($(checkbox_focus.nomjq).is(':checked'))
			graphique.series[checkbox_focus.serie].show();
		else
			graphique.series[checkbox_focus.serie].hide();

}

// CLIC SUR LES CASES DE GROUPEMENT DES VALEURS

function clicbox_grouper () {
	if ($('#grouper').is(':checked'))
//	if (document.getElementById('grouper').checked)
	graphique.tooltip.shared = true;
else
	graphique.tooltip.shared = false;
};

// -----------------------------------------
// FORMATAGE DATE ET HEURE
// -----------------------------------------


// Formatage d'une date au format jj/mm/aaaa
function formater_date (heure)
{
	var chaine = undefined;
	var date = undefined;
	var valeur = undefined;

	chaine = "";
	date = new Date (heure * 1000);
	if (date.getDate () < 10)
	{
		chaine += "0";
	}
	chaine += date.getDate () + "/";
	valeur = date.getMonth () + 1;
	if (valeur < 10)
	{
		chaine += "0";
	}
	chaine += valeur + "/" + date.getFullYear ();
	return chaine;
}

// Formatage d'une date au format aaaa-mm-jj
function formater_date_amj (heure)
{
	var chaine = undefined;
	var date = undefined;
	var valeur = undefined;

	date = new Date (heure * 1000);

	jour = "";
	if (date.getDate () < 10)
	{
		jour += "0";
	}
	jour += date.getDate ();

	mois = "";
	if (date.getMonth () < 9)
	{
		mois += "0";
	}
	mois += date.getMonth () + 1;
	
	resultat = date.getFullYear()+'-'+mois+'-'+jour;
	return resultat;
}

// Formatage d'une heure, avec ou sans les secondes
function formater_heure (heure, formater_secondes)
{
	var chaine = undefined;
	var date = undefined;

	chaine = "";
	date = new Date (heure * 1000);
	if (date.getHours () < 10)
	{
		chaine += "0";
	}
	chaine += date.getHours () + ":";
	valeur = date.getMinutes ();
	if (date.getMinutes () < 10)
	{
		chaine += "0";
	}
	chaine += date.getMinutes ();
	if (formater_secondes == true)
	{
		chaine += ":";
		if (date.getSeconds () < 10)
		{
			chaine += "0";
		}
		chaine += date.getSeconds ();
	}
	return chaine;
}

// Formattage de la date ET de l'heure
function formater_date_heure(heure) {
	return formater_date (heure) + " " + formater_heure (heure, true)
}

// Affichage d'une alerte si la différence de temps est importante avec la dernière donnée

function afficher_duree(duree) {
	// Objet de formattage des chiffres
	var formater_chiffre = new Intl.NumberFormat("fr-FR",{ minimumIntegerDigits: 2 });
	
	base = function(ba,se) { return (ba >= se) ? [ba % se, Math.floor(ba / se)] : [ba, 0] ;};
	seco = base(Math.round(duree/1000),60);
	minu = base(seco[1],60);
	heur = base(minu[1],24);
	if(heur[1] > 0)
		return heur[1]+'j '+formater_chiffre.format(heur[0])+':'+formater_chiffre.format(minu[0])+':'+formater_chiffre.format(seco[0]);
	else
		return formater_chiffre.format(heur[0])+':'+formater_chiffre.format(minu[0])+':'+formater_chiffre.format(seco[0]);
}

// ------------------------------------------------------
// VÉRIFICATION DE L'INTERVALLE AVEC LA DERNIÈRE DONNÉE
// ------------------------------------------------------

function verifier_intervalle_derniere_donnee(heure){

	maintenant = new Date ();
	derniere_donnee_date = new Date (heure * 1000);
	difference = maintenant - derniere_donnee_date;

	// Différence supérieure à 5mn
	if (difference > 10 * 60 * 500 ) {
		libelle_difference = afficher_duree(difference);
		$('#libelle_alerte').html ('Attention, aucune donnée reçue depuis ' + libelle_difference + '<p></p>');
	}
	else 	
	{
		$('#libelle_alerte').html ("");
	}
}

// -----------------------------------------
// INITIALISATION DES CHAMPS DE SAISIE
// -----------------------------------------

function init_saisie() {
	// Sélection par défaut : durée
	$("#duree").prop("checked", true);
	// Durée par défaut
	$("#nb_heures").val (duree_par_defaut);
	
	init_saisie_intervalle();
}

function init_saisie_intervalle() {
	if (derniere_donnee > 10000) {   // Si on a reçu la dernière donnée - sinon ça sera mis à jour au prochain updateChart
		// Si le bouton radio coché est celui de la durée, on met à jour l'intervalle à partir de la dernière donnée reçue
		if ($('input[name=select_donnees]:checked').val() == 'duree') {
				// date de fin = date de la dernière donnée
			$("#date_fin").val (formater_date_amj(derniere_donnee));
			$("#heure_fin").val (formater_heure(derniere_donnee, false));
			// date de fin = date de fin -48h
			dateheure_debut = derniere_donnee - intervalle_par_defaut*3600;
			$("#date_debut").val (formater_date_amj(dateheure_debut));
			$("#heure_debut").val (formater_heure(dateheure_debut,false));
		}
	}
}

// -----------------------------------------
// EXÉCUTION PRINCIPALE
// -----------------------------------------

// Liste des cases à cocher avec leur valeur par défaut, leurs noms jquery, leur id et l'indice de leur série
const reference_checkbox = [
		{ "nomjq" : "#check_t1" , "id" : "check_t1", "valeur" : true, "serie" : 0 },
		{ "nomjq" : "#check_t2" , "id" : "check_t2", "valeur" : false, "serie" : 1 },
		{ "nomjq" : "#check_t3" , "id" : "check_t3", "valeur" : true, "serie" : 2 },
		{ "nomjq" : "#check_t4" , "id" : "check_t4", "valeur" : true, "serie" : 3 },
		{ "nomjq" : "#check_t7" , "id" : "check_t7", "valeur" : false, "serie" : 4 },
		{ "nomjq" : "#check_t8" , "id" : "check_t8", "valeur" : false, "serie" : 5 },
		{ "nomjq" : "#check_t9" , "id" : "check_t9", "valeur" : true, "serie" : 6 },
		{ "nomjq" : "#check_t11" , "id" : "check_t11", "valeur" : true, "serie" : 7 },
		{ "nomjq" : "#check_tcons1" , "id" : "check_tcons1", "valeur" : false, "serie" : 8 },
		{ "nomjq" : "#check_v3v" , "id" : "check_v3v", "valeur" : false, "serie" : 9 },
		{ "nomjq" : "#check_c1" , "id" : "check_c1", "valeur" : false, "serie" : 10 },
		{ "nomjq" : "#check_c4" , "id" : "check_c4", "valeur" : false, "serie" : 11 },
		{ "nomjq" : "#check_c5" , "id" : "check_c5", "valeur" : false, "serie" : 12 },
		{ "nomjq" : "#check_chaudiere" , "id" : "check_chaudiere", "valeur" : false, "serie" : 13 }
	];	

var derniere_donnee = 0;  // max(timesamp) en base mysql, mis à jour par updateChart() et utilisé par init_saisie();

// Durée par défaut dépend de la taille de l'écran : 24h sur PC ou 6h sur mobile
var duree_par_defaut = 24;
var iWindowsSize = $(window).width();
if (iWindowsSize  < 700) {
	duree_par_defaut = 12;
}
var intervalle_par_defaut = 48;

var graphique = Highcharts.chart('container', {});

// initialisation des cases à cocher
init_checkbox();
// initialisation des champs de saisie
init_saisie();
// fabrication du premier graphique
updateChart();

// Test appui sur entrée => on recharge le graphique
$('#nb_heures').keypress(function(e){
    if( e.which == 13 ){
    		updateChart();
    }
});


