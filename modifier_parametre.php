<?php

include("includes/config.inc.php"); 

$cookie = "solisart.cookie";

// --------------------------------
// FONCTION D'AUTHENTIFICATION
// --------------------------------

function authentifier($cookie,$serveur_solisart,$login,$passwd)
{
	// on "force" en POST les variables d'identification et on stocke le cookie obtenu.
	$ch = curl_init();
	
	curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
	curl_setopt($ch, CURLOPT_URL, $serveur_solisart."/admin/index.php");
	curl_setopt($ch, CURLOPT_POST, 1);
	$post_fields = "connexion=Connexion&id=".$login."&pass=".$passwd;
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);

	curl_exec ($ch);
	curl_close ($ch);
}


// --------------------------------
// DÉBUT
// --------------------------------


$type_url = $_GET["type"];

if($type_url == "ecs")
	$type="156";
else
	if($type_url == "chauffage") 
		$type="150";
	else
		die("Mauvais type");

$commande_url = $_GET["commande"];

if($commande_url == "arret")
	$commande=base64_encode(0);
else
	if($commande_url == "appoint") 
		$commande=base64_encode(2);
	else
		if($commande_url == "solaire") 
			$commande=base64_encode(1);
		else
			die("Mauvaise commande");



$commande_xml = '<valeurs><valeur donnee="'.$type.'" valeur="'.$commande.'" /></valeurs>';

$data = array('id' => base64_encode($numero_installation), 'xml' => base64_encode($commande_xml));

// AUTHENTIFICATION
//L'authentification n'est pas nécessaire si on passe au préalable sur la page direct.php (le cookie reste valide)

authentifier($cookie,$serveur_solisart,$login,$passwd);

// POST DE LA COMMANDE

$ch = curl_init();

curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
curl_setopt($ch, CURLOPT_URL, $serveur_solisart."/admin/divers/ajax/ecriture_valeurs_donnees.php");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

$result = curl_exec ($ch);
curl_close ($ch);

// Récupération des données XML
$donnees = simplexml_load_string($result);

// Récupération du statut
$statut = $donnees->attributes()->statut;

// Si échec on affiche le message d'erreur
if ($statut == 'echec')
	echo base64_decode($donnees->attributes()->message);

?>