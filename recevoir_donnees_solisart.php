<?php

include("includes/config.inc.php"); 

$db = new mysqli($db_host, $db_user, $db_pwd, $db_name);


$variables = array (
	'V584' => 't1',
	'V585' => 't2',
	'V586' => 't3',
	'V587' => 't4',
	'V590' => 't7',
	'V591' => 't8',
	'V592' => 't9',
	'V594' => 't11',
	'V606' => 'chaudiere',
	'V614' => 'c4',
	'V615' => 'c5',
	'V618' => 'c1',
	'V628' => 'v3v',
	'V631' => 'tcons1',
	'V985' => 'timestamp'
);

// Récupération des données

$insert_string_fields = '';
$insert_string_values = '';

// Boucle sur la table des valeurs à récupérer
foreach ($variables as $donnee=>$nom)
{
	$valeur = $_GET[$donnee];

	if (is_numeric($valeur))
	{
//			echo $donnee." ".$nom."=".$valeur."<br />";
	
		$insert_string_fields .= ", ".$nom;
		$insert_string_values .= ", ".$valeur;

		// Récupération du timestamp renvoyé par le serveur
		if ($nom = 'timestamp')
			$timestamp = $valeur;
	}
}


// On vérifie que le timestamp n'existe pas déjà en base

$requete_verif = "SELECT timestamp FROM solisgraph WHERE timestamp = ".$timestamp;
$result = $db->query($requete_verif);
if (!$result)
	echo mysqli_error($db);
else
{
	$row = $result->fetch_assoc();

	if (is_null($row))
	{
		// Insertion des données dans la base mysql
	
		$requete = "INSERT INTO solisgraph
			(" . substr($insert_string_fields,1) . ")
			VALUES (" . substr($insert_string_values,1) . ")";
		
//			echo $requete."<br />";
	
		if (!$db->query($requete))
			echo mysqli_error($db);
	}
}


?>