<?php

/* Supprime l'historique Solisgraph au delà de 90 jours */

include("includes/config.inc.php"); 

$db = new mysqli($db_host, $db_user, $db_pwd, $db_name);

$requete = "DELETE FROM `solisgraph` WHERE timestamp <= UNIX_TIMESTAMP(DATE_ADD(CURDATE(),INTERVAL -90 DAY))";

//echo $requete."<br/>";

if (!$db->query($requete))
echo mysqli_error($db);

?>