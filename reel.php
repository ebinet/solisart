<?php
 
include("includes/smarty.inc.php"); 
include("includes/config.inc.php");
 
$cookie = "solisart.cookie";

$libelle = array (
	150 => 'Consigne chauffage',
	156 => 'Consigne ECS',
	157 => 'Temp consigne HP chauffage',
	163 => 'Temp consigne HP ECS',
	164 => 'Temp consigne HC chauffage',
	170 => 'Temp consigne HC ECS',
	584 => 'T1 Capt Chaud 1',
	585 => 'T2 Capt Froid',
	586 => 'T3 Bal Solaire',
	587 => 'T4 Bal Appoint',
	590 => 'T7 Coll Froid',
	591 => 'T8 Coll Chaud',
	592 => 'T9 Exterieure',
	594 => 'T11 Maison',
	606 => 'Chaudière',
	614 => 'C4 BAL Appoint',
	615 => 'C5 BAL Solaire',
	618 => 'C1 ZONE 1',
	628 => 'Position de la V3V appoint',
	981 => 'CarteCompteur',
	982 => 'CarteHeure',
	983 => 'CarteHeureRelative',
	984 => 'ServeurCompteur',
	985 => 'ServeurHeure',
	986 => 'ServeurHeureRelative'
	);

$libelle_consigne = array (
	0 => 'Arrêt',
	1 => 'Solaire',
	2 => 'Appoint'
	);

$libelle_chaudiere = array (
	'On' => 'Marche',
	'Off' => 'Arrêt'
	);


// --------------------------------
// FONCTION D'AUTHENTIFICATION
// --------------------------------

function authentifier($cookie,$serveur_solisart,$login,$passwd)
{
	// on "force" en POST les variables d'identification et on stocke le cookie obtenu.
	$ch = curl_init();
	
	curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
	curl_setopt($ch, CURLOPT_URL, $serveur_solisart."/admin/index.php");
	curl_setopt($ch, CURLOPT_POST, 1);
	$post_fields = "connexion=Connexion&id=".$login."&pass=".$passwd;
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);

	curl_exec ($ch);
	curl_close ($ch);
}

// --------------------------------
// FONCTION DE LECTURE DES DONNÉES
// --------------------------------

function lire_donnees($cookie,$serveur_solisart)
{
	
	$ch = curl_init();
	
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
	curl_setopt($ch, CURLOPT_URL, $serveur_solisart."/admin/divers/ajax/lecture_valeurs_donnees.php");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "id=".base64_encode($numero_installation)."&heure=".strtotime("-1 day")."&periode=1");
	
	$result = curl_exec ($ch);
	curl_close ($ch);
	
	// Récupération des données XML
	$donnees = simplexml_load_string($result);

	return $donnees;

}


// --------------------------------
// FONCTION D'ENVOI DES DONNÉES
// --------------------------------

function envoyer_donnees($cookie,$serveur_solisart,$xml)
{ 
	$data = array('id' => base64_encode($numero_installation), 'xml' => base64_encode($xml));

	$ch = curl_init();
	
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
	curl_setopt($ch, CURLOPT_URL, $serveur_solisart."/admin/divers/ajax/ecriture_valeurs_donnees.php");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	
	$result = curl_exec ($ch);
	curl_close ($ch);
	
	// Récupération des données XML
	$donnees = simplexml_load_string($result);
	
	// Récupération du statut
	$statut = $donnees->attributes()->statut;
	
	// Si échec on affiche le message d'erreur
	if ($statut == 'echec')
		echo base64_decode($donnees->attributes()->message);

}

// --------------------------------
// DÉBUT
// --------------------------------

//--------------------------------------------
// Modifications de consignes
//--------------------------------------------

if (!empty($_POST))
{
	if (isset($_POST['consigne-chauffage']))
	{  
		$valeur_mode_chauffage = $_POST['consigne-chauffage'];
		$consigne_hp_chauffage = str_replace(',','.',$_POST['consigne-hp-chauffage']);
		$consigne_hc_chauffage = str_replace(',','.',$_POST['consigne-hc-chauffage']);
		$commande_xml = '<valeurs>';
		$commande_xml .= '<valeur donnee="150" valeur="'.base64_encode($valeur_mode_chauffage).'" />';
		$commande_xml .= '<valeur donnee="157" valeur="'.base64_encode($consigne_hp_chauffage).'" />';
		$commande_xml .= '<valeur donnee="164" valeur="'.base64_encode($consigne_hc_chauffage).'" />';
		$commande_xml .= '</valeurs>';
		envoyer_donnees($cookie,$serveur_solisart,$commande_xml);
	}

	if (isset($_POST['consigne-ecs']))
	{
		$valeur_mode_ecs = $_POST['consigne-ecs'];
		$consigne_hp_ecs = str_replace(',','.',$_POST['consigne-hp-ecs']);
		$consigne_hc_ecs = str_replace(',','.',$_POST['consigne-hc-ecs']);
		$commande_xml = '<valeurs>';
		$commande_xml .= '<valeur donnee="156" valeur="'.base64_encode($valeur_mode_ecs).'" />';
		$commande_xml .= '<valeur donnee="163" valeur="'.base64_encode($consigne_hp_ecs).'" />';
		$commande_xml .= '<valeur donnee="170" valeur="'.base64_encode($consigne_hc_ecs).'" />';
		$commande_xml .= '</valeurs>';
		envoyer_donnees($cookie,$serveur_solisart,$commande_xml);
	}

/*
	$url_consigne = "https://solaire.vauxclairs.fr/controle/modifier_parametre.php?type=".$type."&commande=".$libelle_commande[$consigne];

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url_consigne);
	curl_exec ($ch);
	curl_close ($ch);
*/
	// On attend quelques secondes avant de relire les données
//	sleep(2);

}


//--------------------------------------------
// Affichage des valeurs
//--------------------------------------------


// Par défaut on appelle la lecture des données sans authentification
// car on présume que c'est déjà fait

$insert_string_fields = '';
$insert_string_values = '';

//$donnees = lire_donnees($cookie,$serveur_solisart);
$donnees = false;

if (!($donnees === false))	
{
	// Récupération du statut
	$statut = $donnees->attributes()->statut;

	// Si échec on fait l'authentification et on relit les données
	if ($statut == 'echec')
	{
		authentifier($cookie,$serveur_solisart,$login,$passwd);
		$donnees = lire_donnees($cookie,$serveur_solisart);
	}

	$timestamp = intval($donnees->attributes()->heure_contact);

	//print_r($donnees); die();
	//echo "<br />";
	
	//$valeurs = $donnees->valeur->attributes();
	
	//echo "<table>";
	
	$valeurs = array();
	
	foreach ($donnees as $donnee)
	{
	/*
			echo "<tr>";
			echo "<td>".$donnee['donnee']."&nbsp&nbsp&nbsp</td>";
			echo "<td>&nbsp&nbsp&nbsp".$libelle[intval($donnee['donnee'])]." &nbsp&nbsp&nbsp</td>";
			echo "<td>&nbsp&nbsp&nbsp".base64_decode($donnee['valeur'])." &nbsp&nbsp&nbsp</td>";
			echo "<td>&nbsp&nbsp&nbsp".date("Y-m-d H:i:s", intval($donnee['heure']))."</td>";
			echo "</tr>";
	*/
		if ($libelle[intval($donnee['donnee'])] != '')
		{
	/*
			echo "<tr>";
			echo "<td>".$donnee['donnee']."&nbsp&nbsp&nbsp</td>";
			echo "<td>&nbsp&nbsp&nbsp".$libelle[intval($donnee['donnee'])]." &nbsp&nbsp&nbsp</td>";
			echo "<td>&nbsp&nbsp&nbsp".base64_decode($donnee['valeur'])." &nbsp&nbsp&nbsp</td>";
			echo "<td>&nbsp&nbsp&nbsp".date("Y-m-d H:i:s", intval($donnee['heure']))."</td>";
			echo "</tr>";
	*/
			$valeur = base64_decode($donnee['valeur']);
	
			// Remplace le degré
			$valeur = str_replace('d','°',$valeur);
			// Remplace la virgule
			$valeur = str_replace('.',',',$valeur);
			// Remplace le pourcentage
			$valeur = str_replace('pC',' %',$valeur);
	
			// Tableau
			$valeurs[intval($donnee['donnee'])] = $valeur;
	
		}
	//	print_r($donnee);
	//	echo "<br />";
		
	}
	
	// Mise en forme du timestamp
	$valeurs[985] = date("d/m/Y H:i:s", $timestamp);
	
	// Libellé de la chaudière
	$valeurs[606] = $libelle_chaudiere[$valeurs[606]];
	

}

$tpl->assign("valeurs",$valeurs);
$tpl->assign("libelle_consigne",$libelle_consigne);
$tpl->assign("numero_installation",$numero_installation);
$tpl->display("reel-min.tpl");



?>