<?php

include("includes/config.inc.php"); 

$cookie = "solisart.cookie";

// --------------------------------
// FONCTION D'AUTHENTIFICATION
// --------------------------------

function authentifier($cookie,$serveur_solisart,$login,$passwd)
{
	// on "force" en POST les variables d'identification et on stocke le cookie obtenu.
	$ch = curl_init();
	
	curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
	curl_setopt($ch, CURLOPT_URL, $serveur_solisart."/admin/index.php");
	curl_setopt($ch, CURLOPT_POST, 1);
	$post_fields = "connexion=Connexion&id=".$login."&pass=".$passwd;
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);

	curl_exec ($ch);
	curl_close ($ch);
}

// --------------------------------
// FONCTION D'ENVOI DES DONNÉES
// --------------------------------

function envoyer_donnees($cookie,$serveur_solisart,$xml,$numero_installation)
{ 
	$data = array('id' => base64_encode($numero_installation), 'xml' => base64_encode($xml));

	$ch = curl_init();
	
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
	curl_setopt($ch, CURLOPT_URL, $serveur_solisart."/admin/divers/ajax/ecriture_valeurs_donnees.php");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	
	$result = curl_exec ($ch);
	curl_close ($ch);
	
	// Récupération des données XML
	$donnees = simplexml_load_string($result);
	
	// Récupération du statut
	$statut = $donnees->attributes()->statut;
	
	// Si échec on affiche le message d'erreur
	if ($statut == 'echec')
		echo base64_decode($donnees->attributes()->message);

	return $result;

}

// --------------------------------
// DÉBUT
// --------------------------------

if (!empty($_POST))
{
	// Normalement pas besoin de s'authentifier puisqu'on interroge le serveur régulièrement
	//authentifier($cookie,$serveur_solisart,$login,$passwd);

	if($_POST['mode'] == 'chauffage')
	{
		$valeur_mode_chauffage = $_POST['consigne_chauffage'];
		$consigne_hp_chauffage = str_replace(',','.',$_POST['consigne_hp_chauffage']);
		$consigne_hc_chauffage = str_replace(',','.',$_POST['consigne_hc_chauffage']);
		$commande_xml = '<valeurs>';
		$commande_xml .= '<valeur donnee="150" valeur="'.base64_encode($valeur_mode_chauffage).'" />';
		$commande_xml .= '<valeur donnee="157" valeur="'.base64_encode($consigne_hp_chauffage).'" />';
		$commande_xml .= '<valeur donnee="164" valeur="'.base64_encode($consigne_hc_chauffage).'" />';
		$commande_xml .= '</valeurs>';
		echo envoyer_donnees($cookie,$serveur_solisart,$commande_xml,$numero_installation);
	}

	if($_POST['mode'] == 'ecs')
	{
		$valeur_mode_ecs = $_POST['consigne_ecs'];
		$consigne_hp_ecs = str_replace(',','.',$_POST['consigne_hp_ecs']);
		$consigne_hc_ecs = str_replace(',','.',$_POST['consigne_hc_ecs']);
		$commande_xml = '<valeurs>';
		$commande_xml .= '<valeur donnee="156" valeur="'.base64_encode($valeur_mode_ecs).'" />';
		$commande_xml .= '<valeur donnee="163" valeur="'.base64_encode($consigne_hp_ecs).'" />';
		$commande_xml .= '<valeur donnee="170" valeur="'.base64_encode($consigne_hc_ecs).'" />';
		$commande_xml .= '</valeurs>';
		echo envoyer_donnees($cookie,$serveur_solisart,$commande_xml,$numero_installation);
	}
}

?>