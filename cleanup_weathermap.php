<?php

/* Supprime l'historique Weathermap au delà de 15 jours */

include("includes/config.inc.php"); 

$db = new mysqli($db_host, $db_user, $db_pwd, $db_name);
		
$requete = "DELETE FROM `weathermap` WHERE timestamp <= UNIX_TIMESTAMP(DATE_ADD(CURDATE(),INTERVAL -15 DAY))";

//echo $requete."<br/>";

if (!$db->query($requete))
echo mysqli_error($db);

?>